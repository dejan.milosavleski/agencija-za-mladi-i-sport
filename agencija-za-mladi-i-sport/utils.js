import _get from 'lodash/get';

export const goTo = (location) => {
  document.location.href = location;
};

export const getLevelPath = (pathname, pathLevel) => {
  const paths = pathname.split('/');
  return _get(paths, `[${pathLevel}]`);
};

export const navigateTo = (url) => {
  window.open(url);
};

export const getCategoryTitle = (category, subcategory) => {
  if (subcategory) {
    return subcategory.name;
  }
  return category ? category.name : '';
};
