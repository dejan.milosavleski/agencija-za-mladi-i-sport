import React from 'react';
import { projectsDataUrl } from '../../contexts/constants';
import { SingleItemPage } from '../../components/single-item-page/SingleItemPage';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import errorData from '../500Error';

export default function SingleProject({ project, projects }) {
  const { t } = useTranslation('common');

  const pageTitle = t('project');
  const rightSectionTitle = t('restOfProjects');
  return (
    <SingleItemPage
      title={pageTitle}
      item={project}
      rightSectionTitle={rightSectionTitle}
      rightSectionItems={projects}
    />
  );
}

export async function getStaticPaths({ locales }) {
  let projects = [];

  const res = await fetch(projectsDataUrl);
  projects = await res.json();

  const paths = [];
  projects.forEach((project) => {
    locales.forEach((wantedLocale) => {
      const path = {
        params: { slug: project.slug },
        locale: wantedLocale
      };
      paths.push(path);
    });
  });

  return { paths, fallback: false };
}

export async function getStaticProps({ params, locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);

  try {
    const res = await fetch(`https://ams.altius.digital/wp-json/v1/project/${params.slug}`);
    const res2 = await fetch(projectsDataUrl);
    const project = await res.json();
    const projects = await res2.json();

    return {
      props: {
        project,
        projects,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        project: errorData,
        ...translationData
      }
    };
  }
}
