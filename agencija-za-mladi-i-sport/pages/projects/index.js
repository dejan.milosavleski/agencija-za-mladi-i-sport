import React from 'react';
import { ArticleList } from '../../components/projects-page/ArticleList';
import { projectsDataUrl } from '../../contexts/constants';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import errorData from '../500Error';

export default function Projects({ projects }) {
  const { t } = useTranslation('common');

  const areaTitle = t('projects');
  const areaSubtitle = t('lookAtProjectsText');

  const fetchMoreArticlesUrl = (page) => `https://ams.altius.digital/wp-json/v1/projects?limit=10&page=${page}`;

  return (
    <ArticleList
      articles={projects}
      areaTitle={areaTitle}
      areaSubtitle={areaSubtitle}
      fetchMoreArticlesUrl={fetchMoreArticlesUrl}
    />
  );
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);

  try {
    const res = await fetch(projectsDataUrl);
    const projects = await res.json();

    return {
      props: {
        projects,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        projects: errorData,
        ...translationData
      }
    };
  }
}
