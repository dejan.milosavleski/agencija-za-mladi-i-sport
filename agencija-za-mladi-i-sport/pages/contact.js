import React from 'react';
import { ContactPage } from '../components/contact-page/ContactPage';
import { contactDataUrl } from '../contexts/constants';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import errorData from './500Error';

export default function Contact({ data }) {
  return <ContactPage data={data} />;
}

export async function getStaticProps({ params, locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res = await fetch(contactDataUrl);
    const contact = await res.json();

    return {
      props: {
        data: contact,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        data: errorData,
        ...translationData
      }
    };
  }
}
