import React from 'react';
import { PublicInfoAndCalls } from '../../../components/public-info-and-calls-page/PublicInfoAndCalls';
import { publicCallsCategoryDataUrl } from '../../../contexts/constants';
import { useRouter } from 'next/router';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import errorData from '../../500Error';
import { useTranslation } from 'next-i18next';

export default function PublicCalls({ publicCalls, categories }) {
  const router = useRouter();
  const { category } = router.query;
  const fetchMoreItemsUrl = (page, status) =>
    `https://ams.altius.digital/wp-json/v1/public-calls?status=${status}&limit=10&page=${page}&category=${category}`;

  const { t } = useTranslation('common');
  const pageTitle = t('publicCalls');

  return (
    <PublicInfoAndCalls
      categories={categories}
      currentCategory={category}
      firstPageName="public-call"
      categoriesPageName="public-calls"
      items={publicCalls}
      isCalls
      title={pageTitle}
      fetchMoreItemsFunctionUrl={fetchMoreItemsUrl}
    />
  );
}

export async function getStaticPaths({ locales }) {
  let categories = [];

  const res = await fetch(publicCallsCategoryDataUrl);
  categories = await res.json();

  const paths = [];
  categories.forEach((category) => {
    locales.forEach((wantedLocale) => {
      const path = {
        params: { category: category.slug },
        locale: wantedLocale
      };
      paths.push(path);
    });
  });

  return { paths, fallback: false };
}

export async function getStaticProps({ params, locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res = await fetch(
      `https://ams.altius.digital/wp-json/v1/public-calls?status=1&limit=10&page=1&category=${params.category}`
    );
    const res2 = await fetch(publicCallsCategoryDataUrl);
    const publicCalls = await res.json();
    const categories = await res2.json();

    return {
      props: {
        publicCalls,
        categories,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        publicCalls: errorData,
        categories: errorData,
        ...translationData
      }
    };
  }
}
