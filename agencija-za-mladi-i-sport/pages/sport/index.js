import React from 'react';
import { SportYoungPage } from '../../components/sport-young-page/SportYoungPage';
import { sportPageDataUrl } from '../../contexts/constants';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import errorData from '../500Error';

export default function Sport({ data, relatedNews }) {
  const { t } = useTranslation('common');
  const pageTitle = t('sportPageTitle');

  return <SportYoungPage pageTitle={pageTitle} content={data} relatedNews={relatedNews} category="sport" />;
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res = await fetch(sportPageDataUrl);
    const data = await res.json();

    const res2 = await fetch(`https://ams.altius.digital/wp-json/v1/posts/?limit=3&page=1&category=sport`);
    const relatedNews = await res2.json();

    return {
      props: {
        data,
        relatedNews,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        data: errorData,
        relatedNews: errorData,
        ...translationData
      }
    };
  }
}
