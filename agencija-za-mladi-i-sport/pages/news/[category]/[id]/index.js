import React from 'react';
import { NewsAndCategories } from '../../../../components/news-page/NewsAndCategories';
import { useRouter } from 'next/router';
import _get from 'lodash/get';
import { NewsContent } from '../../../../components/news-page/NewsContent';
import { RelatedNews } from '../../../../components/projects-page/RelatedNews';
import _isEmpty from 'lodash/isEmpty';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import errorData from '../../../500Error';

export default function SingleNewsArticle({ newsItem, categories }) {
  const router = useRouter();
  const { category } = router.query;

  const { t } = useTranslation('common');
  const relatedNewsLabel = t('relatedNews');
  const related_content = _get(newsItem, 'related_content');
  const imageUrl = _get(newsItem, 'imageUrl');

  return (
    <div>
      <NewsAndCategories imageUrl={imageUrl} categories={categories} currentCategory={category} firstPageName="news" />
      <NewsContent newsItem={newsItem} />
      <RelatedNews content={related_content} title={relatedNewsLabel} />
    </div>
  );
}

export async function getStaticPaths({ locales }) {
  let news = [];
  let page = 1;
  let hasMore = true;

  while (hasMore) {
    const res = await fetch(`https://ams.altius.digital/wp-json/v1/posts/?limit=100&page=${page}`);
    const resolvedData = await res.json();
    news.push(...resolvedData);
    hasMore = !_isEmpty(resolvedData);
    page++;
  }

  let paths = [];
  news.forEach((item) => {
    const newsSlug = item.slug;
    const newsCategories = item.categories;
    newsCategories.forEach((category) => {
      locales.forEach((wantedLocale) => {
        const paramItem = {
          params: { id: newsSlug, category: category.slug },
          locale: wantedLocale
        };
        paths.push(paramItem);
      });
    });
  });

  return { paths, fallback: true };
}

export async function getStaticProps({ params, locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res = await fetch(encodeURI(`https://ams.altius.digital/wp-json/v1/post/${params.id}`));
    const newsItem = await res.json();
    const res2 = await fetch(encodeURI(`https://ams.altius.digital/wp-json/v1/categories/category`));
    const categories = await res2.json();

    return {
      props: {
        newsItem,
        categories,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        newsItem: errorData,
        ...translationData
      }
    };
  }
}
