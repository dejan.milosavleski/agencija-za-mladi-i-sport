import React from 'react';
import { SingleItemPublic } from '../../components/public-info-and-calls-page/SingleItemPublic';
import _isEmpty from 'lodash/isEmpty';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import errorData from '../500Error';

export default function PubilcCall({ publicCall }) {
  const { t } = useTranslation('common');
  const pageTitle = t('publicCalls');

  return <SingleItemPublic item={publicCall} title={pageTitle} isPublicCall />;
}

export async function getStaticPaths({ locales }) {
  let publicCalls = [];
  let hasMore = true;
  let page = 1;

  while (hasMore) {
    const res = await fetch(`https://ams.altius.digital/wp-json/v1/public-calls?limit=50&page=${page}`);
    const resolvedData = await res.json();
    publicCalls.push(...resolvedData);
    hasMore = !_isEmpty(resolvedData);
    page++;
  }

  const paths = [];
  publicCalls.forEach((pc) => {
    locales.forEach((wantedLocale) => {
      const path = {
        params: { slug: pc.slug },
        locale: wantedLocale
      };
      paths.push(path);
    });
  });

  return { paths, fallback: false };
}

export async function getStaticProps({ params, locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res = await fetch(`https://ams.altius.digital/wp-json/v1/public-call/${params.slug}`);
    const publicCall = await res.json();

    return {
      props: {
        publicCall,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        publicCall: errorData,
        ...translationData
      }
    };
  }
}
