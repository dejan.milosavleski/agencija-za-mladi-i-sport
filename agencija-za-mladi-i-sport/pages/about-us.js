import React from 'react';
import { aboutUsDataUrl } from '../contexts/constants';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { AboutUsPage } from '../components/about-us-page/AboutUsPage';
import errorData from './500Error';

export default function AboutUs({ data }) {
  return <AboutUsPage data={data} />;
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res = await fetch(aboutUsDataUrl);
    const aboutUsData = await res.json();

    return {
      props: {
        data: aboutUsData,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        data: errorData,
        ...translationData
      }
    };
  }
}
