import React from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { youngPageDataUrl } from '../../contexts/constants';
import { RelatedPage } from '../../components/related-page/RelatedPage';
import errorData from '../500Error';

export default function Page({ data }) {
  return <RelatedPage data={data} />;
}

export async function getStaticPaths({ locales }) {
  let pages = [];

  const res = await fetch(youngPageDataUrl);
  const data = await res.json();

  // TODO smihail 2021-10-04: add sport as well when it's fixed on server side
  // const res2 = await fetch(sportPageDataUrl);
  // const data2 = await res.json();

  pages = data.related_pages;

  const paths = [];
  pages.forEach((page) => {
    locales.forEach((wantedLocale) => {
      const path = {
        params: { page: page.post_name },
        locale: wantedLocale
      };
      paths.push(path);
    });
  });

  return { paths, fallback: false };
}

export async function getStaticProps({ params, locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const { page } = params;
    const data = await fetch(`https://ams.altius.digital/wp-json/v1/pages/${page}`);
    const resolvedData = await data.json();
    return {
      props: {
        data: resolvedData,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        data: errorData,
        ...translationData
      }
    };
  }
}
