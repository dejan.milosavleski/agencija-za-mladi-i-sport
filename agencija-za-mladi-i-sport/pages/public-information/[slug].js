import React from 'react';
import { SingleItemPublic } from '../../components/public-info-and-calls-page/SingleItemPublic';
import _isEmpty from 'lodash/isEmpty';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import errorData from '../500Error';
import { useTranslation } from 'next-i18next';

export default function PubilcInformation({ publicInfo }) {
  const { t } = useTranslation('common');
  const pageTitle = t('publicInformation');

  return <SingleItemPublic item={publicInfo} title={pageTitle} />;
}

export async function getStaticPaths({ locales }) {
  let publicInformation = [];
  let hasMore = true;
  let page = 1;

  while (hasMore) {
    const res = await fetch(`https://ams.altius.digital/wp-json/v1/public-infos?limit=50&page=${page}`);
    const resolvedData = await res.json();
    publicInformation.push(...resolvedData);
    hasMore = !_isEmpty(resolvedData);
    page++;
  }

  const paths = [];
  publicInformation.forEach((pc) => {
    locales.forEach((wantedLocale) => {
      const path = {
        params: { slug: pc.slug },
        locale: wantedLocale
      };
      paths.push(path);
    });
  });

  return { paths, fallback: false };
}

export async function getStaticProps({ params, locale }) {
  let publicInfo;
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res = await fetch(`https://ams.altius.digital/wp-json/v1/public-info/${params.slug}`);
    publicInfo = await res.json();
  } catch (e) {
    publicInfo = errorData;
  }
  return {
    props: {
      publicInfo,
      ...translationData
    }
  };
}
