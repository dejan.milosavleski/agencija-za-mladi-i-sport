import React from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { ServerErrorPage } from '../components/500ErrorPage';

export default function NotFound() {
  return <ServerErrorPage />;
}

export async function getStaticProps({ params, locale }) {
  try {
    return {
      props: {
        ...(await serverSideTranslations(locale, ['common']))
      }
    };
  } catch (e) {
    return {
      props: {}
    };
  }
}
