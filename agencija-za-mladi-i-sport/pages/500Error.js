const errorData = {
  code: 500,
  message: 'getStaticProps error'
};

export default errorData;
