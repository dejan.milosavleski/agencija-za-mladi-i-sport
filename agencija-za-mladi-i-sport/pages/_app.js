import React from 'react';
import { appWithTranslation } from 'next-i18next';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/globals.css';
import Page from '../components/Page';
import { WithLocaleContext } from '../contexts/LocaleContext';
import _get from 'lodash/get';
import { ServerErrorPage } from '../components/500ErrorPage';

function MyApp({ Component, pageProps }) {
  const props = _get(Object.values(pageProps), '[0]', {});
  const serverError = props.code === 500;

  return (
    <WithLocaleContext>
      <Page>{serverError ? <ServerErrorPage /> : <Component {...pageProps} />}</Page>
    </WithLocaleContext>
  );
}

export default appWithTranslation(MyApp);
