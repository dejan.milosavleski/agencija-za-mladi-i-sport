import React from 'react';
import _get from 'lodash/get';
import { PublicInfoAndCalls } from '../../../components/public-info-and-calls-page/PublicInfoAndCalls';
import { publicInfoCategoryDataUrl } from '../../../contexts/constants';
import { useRouter } from 'next/router';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import errorData from '../../500Error';
import { useTranslation } from 'next-i18next';

export default function PublicInfo({ publicInfos, categories }) {
  const router = useRouter();
  const { category } = router.query;

  const fetchMoreItemsUrl = (page) =>
    `https://ams.altius.digital/wp-json/v1/public-infos?limit=8&page=${page}&category=${category}`;

  const { t } = useTranslation('common');
  const pageTitle = t('publicInformation');

  return (
    <PublicInfoAndCalls
      categories={categories}
      currentCategory={category}
      firstPageName="public-information"
      categoriesPageName="public-info"
      items={publicInfos}
      title={pageTitle}
      fetchMoreItemsUrlFunction={fetchMoreItemsUrl}
    />
  );
}

export async function getStaticPaths({ locales }) {
  let categories = [];

  const res = await fetch(publicInfoCategoryDataUrl);
  categories = await res.json();

  const paths = [];
  categories.forEach((category) => {
    locales.forEach((wantedLocale) => {
      const path = {
        params: { category: category.slug },
        locale: wantedLocale
      };
      paths.push(path);
    });
  });

  return { paths, fallback: false };
}

export async function getStaticProps({ params, locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res = await fetch(
      `https://ams.altius.digital/wp-json/v1/public-infos?limit=10S&page=1&category=${params.category}`
    );
    const res2 = await fetch(publicInfoCategoryDataUrl);
    const publicInfos = await res.json();
    const categories = await res2.json();

    return {
      props: {
        publicInfos,
        categories,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        publicInfos: errorData,
        ...translationData
      }
    };
  }
}
