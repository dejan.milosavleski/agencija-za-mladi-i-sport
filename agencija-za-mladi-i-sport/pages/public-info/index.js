import React from 'react';
import { publicInfoCategoryDataUrl, publicInfoDataUrl } from '../../contexts/constants';
import { PublicInfoAndCalls } from '../../components/public-info-and-calls-page/PublicInfoAndCalls';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import errorData from '../500Error';

export default function PublicCalls({ publicInformation, categories }) {
  const fetchMoreItemsUrl = `https://ams.altius.digital/wp-json/v1/
  public-infos?limit=10&page=`;
  return (
    <PublicInfoAndCalls
      categories={categories}
      currentCategory=""
      firstPageName="public-information"
      categoriesPageName="public-info"
      items={publicInformation}
      title="Информации од јавен карактер"
      fetchMoreItemsUrl={fetchMoreItemsUrl}
      test
    />
  );
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res = await fetch(publicInfoDataUrl);
    const res2 = await fetch(publicInfoCategoryDataUrl);
    const publicInformation = await res.json();
    const categories = await res2.json();

    return {
      props: {
        publicInformation,
        categories,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        publicInformation: errorData,
        categories: errorData,
        ...translationData
      }
    };
  }
}
