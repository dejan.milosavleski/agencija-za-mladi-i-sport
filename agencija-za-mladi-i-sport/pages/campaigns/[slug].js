import React from 'react';
import { campaignsDataUrl } from '../../contexts/constants';
import { SingleItemPage } from '../../components/single-item-page/SingleItemPage';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import errorData from '../500Error';

export default function Campaigns({ campaign, campaigns }) {
  const { t } = useTranslation('common');

  const pageTitle = t('campaign');
  const rightSectionTitle = t('restOfCampaigns');

  return (
    <div>
      <SingleItemPage
        title={pageTitle}
        item={campaign}
        rightSectionTitle={rightSectionTitle}
        rightSectionItems={campaigns}
      />
    </div>
  );
}

export async function getStaticPaths({ locales }) {
  let projects = [];

  const res = await fetch(campaignsDataUrl);
  projects = await res.json();

  const paths = [];
  projects.forEach((project) => {
    locales.forEach((wantedLocale) => {
      const path = {
        params: { slug: project.slug },
        locale: wantedLocale
      };
      paths.push(path);
    });
  });

  return { paths, fallback: false };
}

export async function getStaticProps({ params, locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);

  try {
    const res = await fetch(`https://ams.altius.digital/wp-json/v1/campaign/${params.slug}`);
    const res2 = await fetch(campaignsDataUrl);
    const campaign = await res.json();
    const campaigns = await res2.json();

    return {
      props: {
        campaign,
        campaigns,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        campaign: errorData,
        ...translationData
      }
    };
  }
}
