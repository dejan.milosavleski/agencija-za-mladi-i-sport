# Agencija za mladi i sport - Web application for news related with agency of youth and sport

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Available Commands

| Command                     | Description                                                                            |
| --------------------------- | --------------------------------------------------------------------------------------|
| `yarn dev`                  | Runs next dev which starts Next.js in development mode                                                  |
| `yarn build`                | Runs next build which builds the application for production usage                                     |
| `yarn start`                | Runs next start which starts a Next.js production server                                              |
| `yarn lint`                 | Runs next lint which sets up Next.js' built-in ESLint configuration.                                  |


## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

