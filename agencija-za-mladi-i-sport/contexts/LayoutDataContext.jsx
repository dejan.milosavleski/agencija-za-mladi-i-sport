import React from 'react';
import { footerDataUrl, headerDataUrl } from './constants';

export const LayoutDataContext = React.createContext({});
LayoutDataContext.displayName = 'LayoutDataContextType';

export const WithLayoutDataContext = ({ children }) => {
  const [headerData, setHeaderData] = React.useState({});
  const [footerData, setFooterData] = React.useState({});

  // TODO smihail 2021-09-17: change async calls when you get back to data handling
  React.useEffect(async () => {
    const fetchedHeaderData = await fetch(headerDataUrl).then((response) => response.json());
    setHeaderData(fetchedHeaderData);
    const fetchedFooterData = await fetch(footerDataUrl).then((response) => response.json());
    setFooterData(fetchedFooterData);
  }, []);

  return <LayoutDataContext.Provider value={{ headerData, footerData }}>{children}</LayoutDataContext.Provider>;
};
