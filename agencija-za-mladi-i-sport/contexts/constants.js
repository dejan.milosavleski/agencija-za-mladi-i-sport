export const headerDataUrl = 'https://ams.altius.digital/wp-json/v1/header';
export const footerDataUrl = 'https://ams.altius.digital/wp-json/v1/footer';
export const frontPageDataUrl = 'https://ams.altius.digital/wp-json/v1/pages/front';
export const projectsDataUrl = 'https://ams.altius.digital/wp-json/v1/projects';
export const campaignsDataUrl = 'https://ams.altius.digital/wp-json/v1/campaigns?limit=10&page=1';
export const aboutUsDataUrl = 'https://ams.altius.digital/wp-json/v1/page/about-us';
export const youngPageDataUrl = 'https://ams.altius.digital/wp-json/v1/pages/young';
export const sportPageDataUrl = 'https://ams.altius.digital/wp-json/v1/pages/sport';
export const newsCategoryDataUrl = 'https://ams.altius.digital/wp-json/v1/categories/category';
export const publicInfoCategoryDataUrl = 'https://ams.altius.digital/wp-json/v1/categories/categories_public_info';
export const publicCallsCategoryDataUrl = 'https://ams.altius.digital/wp-json/v1/categories/categories_public_calls';
export const publicInfoDataUrl = 'https://ams.altius.digital/wp-json/v1/public-infos?limit=10&page=1';
export const publicCallsDataUrl = 'https://ams.altius.digital/wp-json/v1/public-calls?status=1&limit=10&page=1';
export const contactDataUrl = 'https://ams.altius.digital/wp-json/v1/page/contact';

export const mapAccessToken =
  'pk.eyJ1Ijoic21paGFpbCIsImEiOiJja3UxZGMzeGgxMW4yMm9wYzFuMmkzdzNjIn0.DWcXBsX3k2jQFVvwzzSYGA';
