module.exports = {
  i18n: {
    locales: ['mk', 'sq', 'en'],
    defaultLocale: 'mk',
    localeDetection: false
  }
};
