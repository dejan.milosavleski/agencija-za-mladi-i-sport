import React from 'react';
import styled from '@emotion/styled';
import _isNil from 'lodash/isNil';
import { getLevelPath } from '../utils';

// Components
import { Header } from './layout/header/Header';
import { Navigation } from './layout/navigation/Navigation';
import { Footer } from './layout/footer/Footer';

// Context
import { WithLayoutDataContext } from '../contexts/LayoutDataContext';

// Hooks
import { useRouter } from 'next/router';

const Content = styled.div`
  min-height: 600px;
`;

const NEWS_PATH = 'news';
const PUBLIC_CALLS_PATH = 'public-calls';
const PUBLIC_INFO_PATH = 'public-info';
const PROJECTS_PATH = 'projects';
const CAMPAIGNS_PATH = 'campaigns';

const Page = ({ children }) => {
  const router = useRouter();
  const { asPath } = router;
  const firstLevelPath = getLevelPath(asPath, 1);
  const thirdLevelPath = getLevelPath(asPath, 3);

  // TODO smihail 2021-10-08: this was reported as a defect. Communicate how to proceed.
  // const noFooterCurrent =
  //   (firstLevelPath === NEWS_PATH && _isNil(thirdLevelPath)) ||
  //   firstLevelPath === PUBLIC_CALLS_PATH ||
  //   firstLevelPath === PUBLIC_INFO_PATH ||
  //   firstLevelPath === PROJECTS_PATH ||
  //   firstLevelPath === CAMPAIGNS_PATH;

  return (
    <WithLayoutDataContext>
      <Header />
      <Navigation />
      <Content>{children}</Content>
      <Footer />
    </WithLayoutDataContext>
  );
};

export default Page;
