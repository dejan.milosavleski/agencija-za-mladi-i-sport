import React from 'react';
import _map from 'lodash/map';
import Carousel from 'react-bootstrap/Carousel';
import styled from '@emotion/styled';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { breakpoints } from '../../styles/theme';
import { SliderSubtitle, SliderTitle } from '../common/StyledTypography';

const StyledImage = styled.div`
  height: 23vw;
  width: 46vw;

  background: ${(props) => `linear-gradient(180deg, #00000000 0%, #000000 100%),
    url(${props.url}) no-repeat center`};
  background-size: cover;

  :hover {
    cursor: pointer;
  }

  @media ${breakpoints.lg} {
    min-height: 350px;
    width: 100%;
  }
`;

const StyledCarousel = styled(Carousel)`
  padding-left: 15px;
`;

export const SliderInfo = ({ sliderInfo }) => {
  const [index, setIndex] = React.useState(0);
  const router = useRouter();
  const { locale } = router;

  const handleSelect = (selectedIndex) => {
    setIndex(selectedIndex);
  };

  return (
    <StyledCarousel activeIndex={index} onSelect={handleSelect} interval={3000}>
      {_map(sliderInfo, (si) => {
        return (
          <Carousel.Item key={si.post_name}>
            <StyledImage url={si.thumbnail} onClick={() => goTo(`${locale}/news/all/${si.post_name}`)}>
              <Carousel.Caption>
                <SliderTitle>
                  <Link href={`/news/all/${si.post_name}`}>{si.title}</Link>
                </SliderTitle>
                <SliderSubtitle>{si.excerpt}</SliderSubtitle>
              </Carousel.Caption>
            </StyledImage>
          </Carousel.Item>
        );
      })}
    </StyledCarousel>
  );
};
