import React from 'react';
import Container from 'react-bootstrap/Container';
import { SliderInfo } from './SliderInfo';
import Row from 'react-bootstrap/Row';
import styled from '@emotion/styled';
import Col from 'react-bootstrap/Col';
import { CarouselCol } from '../common/StyledButton';
import { PromotedPublicCallsContent } from '../common/public-calls-and-promotions/PromotedPublicCallsContent';

const StyledContainer = styled(Container)`
  padding: 50px 0;
`;

export const PublicCallsAndPromotions = ({ promotedPublicCalls, sliderInfo, front, title, seeAll }) => {
  const page = 'public-call';
  return (
    <StyledContainer>
      <Row>
        <CarouselCol sm={12} lg={7}>
          <SliderInfo sliderInfo={sliderInfo} />
        </CarouselCol>
        <Col sm={12} lg={5}>
          <PromotedPublicCallsContent
            promotedPublicCalls={promotedPublicCalls}
            front={front}
            title={title}
            seeAll={seeAll}
            page={page}
          />
        </Col>
      </Row>
    </StyledContainer>
  );
};
