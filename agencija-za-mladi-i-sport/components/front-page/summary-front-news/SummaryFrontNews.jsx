import React from 'react';
import styled from '@emotion/styled';
import _map from 'lodash/map';
import { theme } from '../../../styles/theme';

// Components
import Container from 'react-bootstrap/Container';
import Carousel from 'react-bootstrap/Carousel';
import Image from 'next/image';
import { StyledButton } from '../../common/StyledButton';

// icons
import check from '../../../assets/icons/icon-feather-check.svg';

// utils
import { navigateTo } from '../../../utils';

const ImageContainer = styled.div`
  background-image: ${(props) => `linear-gradient(350deg, #FFFFFF 0%, #FFFFFF00 100%) , url(${props.url})`};
  background-repeat: no-repeat;
  background-size: inherit;
  object-fit: cover;

  min-height: 450px;
`;

const FlexContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const FlexColumn = styled.div`
  display: flex;
  padding-right: 15px;
  flex-direction: column;
  margin-top: ${(props) => props.paddingtop}px;
  flex: ${(props) => props.flex};
  align-items: ${(props) => props.align};
`;

const FlexRow = styled.div`
  flex: ${(props) => props.flex};
  margin-top: ${(props) => props.top}px;
`;

const Item = styled(FlexRow)`
  align-items: flex-end;
  display: inline-flex;
  color: ${theme.palette.navGray};
  text-align: end;
  margin-bottom: 20px;
`;

const Title = styled.div`
  font-size: 36px;
  color: ${theme.palette.navGray};
  font-family: StobiSans-Black, sans-serif;
`;

const Subtitle = styled.div`
  font-size: 22px;
  color: ${theme.palette.navGray};
`;

const Number = styled.div`
  text-align: end;
  font-family: StobiSans-Bold, sans-serif;
  font-size: 50px;
  line-height: 60px;
`;

const Description = styled.div`
  font-size: 17px;
`;

const StyledImage = styled.div`
  padding-bottom: 40px;
`;

const ContentContainer = styled(Container)`
  padding-top: 50px;
`;

export const SummaryFrontNews = ({ content }) => {
  return (
    <>
      <Carousel nextIcon={''} prevIcon={''}>
        {_map(content, (item) => {
          const { small_tagline, background_image, button_1, button_2, statistics, big_tagline } = item;
          const { title, url } = background_image;
          const { title: button1Title, url: button1Url, target1 } = button_1;
          const { title: button2Title, url: button2Url, target2 } = button_2;
          return (
            <Carousel.Item>
              <ImageContainer url={url}>
                <ContentContainer>
                  <FlexContainer>
                    <FlexColumn flex={8} paddingtop={40}>
                      <FlexRow>
                        <Title>{big_tagline}</Title>
                      </FlexRow>
                      <FlexRow top={10}>
                        <Subtitle>{small_tagline}</Subtitle>
                      </FlexRow>
                      <FlexRow>
                        <StyledButton minwidth={180} onClick={() => navigateTo(button1Url)}>
                          {button1Title}
                        </StyledButton>
                        <StyledButton
                          minwidth={180}
                          marginleft={isSmall ? 0 : 10}
                          onClick={() => navigateTo(button2Url)}
                        >
                          {button2Title}
                        </StyledButton>
                      </FlexRow>
                    </FlexColumn>
                    <FlexColumn flex={4} align="flex-end">
                      {_map(statistics, (item) => (
                        <Item>
                          <FlexColumn>
                            <Number>{item.number}</Number>
                            <Description>{item.text}</Description>
                          </FlexColumn>
                          <StyledImage>
                            <Image src={check} alt="check" width={25} height={25} />
                          </StyledImage>
                        </Item>
                      ))}
                    </FlexColumn>
                  </FlexContainer>
                </ContentContainer>
              </ImageContainer>
            </Carousel.Item>
          );
        })}
      </Carousel>
    </>
  );
};
