import React from 'react';
import styled from '@emotion/styled';
import { theme } from '../../../styles/theme';

// Components
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import ListGroup from 'react-bootstrap/ListGroup';
import { TitleArea } from '../../common/TitleArea';

// Lodash
import _map from 'lodash/map';
import _get from 'lodash/get';
import _indexOf from 'lodash/indexOf';

const ListTitle = styled.div`
  font-size: 25px;
  color: ${theme.palette.navGray};
  font-family: StobiSans-Bold, sans-serif;
  opacity: ${(props) => (props.black ? '100%' : '54%')};
  :hover {
    cursor: pointer;
  }
`;

const ListSubtitle = styled.div`
  font-size: 16px;
  color: ${theme.palette.navGray};
  font-family: StobiSans-Light, sans-serif;
  opacity: ${(props) => (props.black ? '100%' : '54%')};
  margin: 15px 0;
`;

const Content = styled(Container)`
  padding: 50px 0;
  margin: 30px auto;
`;

const StyledImage = styled.div`
  max-width: 750px;
  height: 100%;
  min-height: 350px;
  background: ${(props) => `url(${props.url}) no-repeat center`};
  background-size: cover;
`;

export const Campaigns = ({ campaigns }) => {
  const { section_description, section_title, campaign_items } = campaigns;

  const firstCampaignId = _get(campaigns, 'campaign_items.[0].ID');
  const [selectedCampaignId, setSelectedCampaignId] = React.useState();

  React.useEffect(() => {
    if (!selectedCampaignId) {
      setSelectedCampaignId(firstCampaignId);
    }
  }, [firstCampaignId, selectedCampaignId]);

  setTimeout(function () {
    const currentIdIndex = _indexOf(
      campaign_items.map((ci) => ci.ID),
      selectedCampaignId
    );

    if (currentIdIndex < campaign_items.length - 1) {
      setSelectedCampaignId(campaign_items[currentIdIndex + 1].ID);
    } else {
      setSelectedCampaignId(campaign_items[0].ID);
    }
  }, 4000);

  const campaignPictureItem = campaign_items.find((campaign) => {
    return campaign.ID === selectedCampaignId;
  });

  const campaignPicture = _get(campaignPictureItem, 'thumbnail');
  return (
    <Content>
      <TitleArea title={section_title} subtitle={section_description} />
      <Row>
        <Col xs={12} lg={7}>
          <StyledImage url={campaignPicture} alt="campaign" />
        </Col>
        <Col xs={12} lg={5}>
          <ListGroup variant="flush">
            {_map(campaign_items, (ci) => (
              <ListGroup.Item key={ci.ID}>
                <ListTitle black={ci.ID === selectedCampaignId} onClick={() => setSelectedCampaignId(ci.ID)}>
                  {ci.title}
                </ListTitle>
                <ListSubtitle black={ci.ID === selectedCampaignId}>{ci.excerpt}</ListSubtitle>
              </ListGroup.Item>
            ))}
          </ListGroup>
        </Col>
      </Row>
    </Content>
  );
};
