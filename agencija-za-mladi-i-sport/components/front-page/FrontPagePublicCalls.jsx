import React from 'react';
import styled from '@emotion/styled';
import { breakpoints, theme } from '../../styles/theme';

// Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import { NewsItemList } from '../common/NewsItemList';
import { TitleArea } from '../common/TitleArea';

// Hooks
import { useTranslation } from 'next-i18next';

const Content = styled.div`
  background-color: ${theme.palette.lightGray};
  padding: 40px 0;
`;

const StyledRow = styled(Row)`
  margin-left: 9vw;
  @media ${breakpoints.md} {
    margin-left: 1vw;
  }
`;

export const FrontPagePublicCalls = ({ publicCalls }) => {
  const { contest_public_calls, scolarships, section_title, section_description } = publicCalls;
  const { t } = useTranslation('common');
  const contestTitle = t('contestsForYoung');
  const seeAllContest = t('seeAllContests');
  const scholarshipTitle = t('scholarships');
  const seeAllScholarship = t('seeAllScholarships');

  return (
    <Content>
      <Container>
        <TitleArea title={section_title} subtitle={section_description} />
        <StyledRow>
          <Col>
            <NewsItemList
              title={contestTitle}
              items={contest_public_calls}
              endItem={{ text: seeAllContest, href: '/public-calls/young/contests' }}
              noBorder
            />
          </Col>
          <Col>
            <NewsItemList
              title={scholarshipTitle}
              items={scolarships}
              endItem={{ text: seeAllScholarship, href: '/public-calls/young/scolarships' }}
              noBorder
            />
          </Col>
        </StyledRow>
      </Container>
    </Content>
  );
};
