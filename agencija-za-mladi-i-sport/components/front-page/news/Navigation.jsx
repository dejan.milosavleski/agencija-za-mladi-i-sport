import React from 'react';
import styled from '@emotion/styled';
import { theme } from '../../../styles/theme';
import _map from 'lodash/map';

// Components
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';

// Hooks
import { useTranslation } from 'next-i18next';

const Title = styled.div`
  font-size: 36px;
  color: ${theme.palette.primaryRed};
  font-family: StobiSans-Bold, sans-serif;
`;

const StyledNavLink = styled.div`
  font-size: 20px;
  margin: 0 5px;
  color: ${theme.palette.navGray};
  padding-bottom: 5px;
  border-bottom: ${(props) => props.selected === 'selected' && `1px solid ${theme.palette.primaryRed}`};
  : hover{
  cursor: pointer;
  color: ${theme.palette.primaryRed};
`;

const StyledNavBar = styled(Nav)`
  margin-right: 8px;
`;

export const NewsNavigation = ({ categories, setNewsToDisplay, setSelectedCategory, selectedCategory }) => {
  const filterAllNewsIfPresent = categories.filter((cat) => cat.slug !== 'all');

  const { t } = useTranslation('common');
  const newsLabel = t('newsLabel');

  const fetchNewsForCategory = React.useCallback(
    async (category) => {
      const fetchFirstNNews = async (category, n) => {
        const res = await fetch(
          `https://ams.altius.digital/wp-json/v1/posts/?limit=${n}&page=0&category=${category.slug}`
        );
        return await res.json();
      };

      const newsToShow = await fetchFirstNNews(category, 3);
      setNewsToDisplay(newsToShow);
      setSelectedCategory(category);
    },
    [setNewsToDisplay, setSelectedCategory]
  );

  return (
    <Navbar>
      <Container>
        <Navbar.Brand>
          <Title>{newsLabel}</Title>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <StyledNavBar>
          {_map(filterAllNewsIfPresent, (cat) => (
            <StyledNavLink
              key={cat.slug}
              onClick={() => fetchNewsForCategory(cat)}
              selected={selectedCategory && cat.slug === selectedCategory.slug ? 'selected' : ''}
            >
              {cat.name}
            </StyledNavLink>
          ))}
        </StyledNavBar>
      </Container>
    </Navbar>
  );
};
