import React from 'react';
import styled from '@emotion/styled';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import _get from 'lodash/get';
import { StyledHtml } from '../common/StyledHtml';

const Title = styled.div`
  font-size: 36px;
  font-family: StobiSans-Bold, sans-serif;
  margin-bottom: 10px;
`;

export const NewsContent = ({ newsItem }) => {
  const title = _get(newsItem, 'title');
  const content = _get(newsItem, 'content');

  return (
    <Container>
      <Row>
        <Col xs={12} sm={9}>
          <Title>{title}</Title>
          <StyledHtml dangerouslySetInnerHTML={{ __html: content }} />
        </Col>
      </Row>
    </Container>
  );
};
