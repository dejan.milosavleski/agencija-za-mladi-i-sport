import React from 'react';
import styled from '@emotion/styled';

// Components
import Container from 'react-bootstrap/Container';
import { TitleArea } from '../common/TitleArea';

const ContainerImage = styled.img`
  width: 100%;
`;

const Content = styled.div`
  margin: 40px 0;
`;

export const Organogram = ({ image, title, subtitle }) => {
  return (
    <Content>
      <TitleArea title={title} subtitle={subtitle} />
      <Container>
        <ContainerImage src={image} alt="organogram" />
      </Container>
    </Content>
  );
};
