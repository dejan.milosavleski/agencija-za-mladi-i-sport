import React from 'react';
import _get from 'lodash/get';
import { useTranslation } from 'next-i18next';
import { AboutUsInfoSection } from './AboutUsInfoSection';
import { SportAdvicePeople } from './SportAdvicePeople';
import { Sectors } from './Sectors';
import { Organogram } from './Organogram';

export const AboutUsPage = ({ data }) => {
  const directors = _get(data, 'directors', []);
  const sectors = _get(data, 'sectors', []);
  const title_sectors = _get(data, 'title_sectors', []);
  const description_sectors = _get(data, 'description_sectors', []);
  const image_organogram = _get(data, 'image_organogram');
  const title_organogram = _get(data, 'title_organogram');
  const description_organogram = _get(data, 'description_organogram');
  const aboutUsDescr = _get(data, 'description_about_us');
  const title_advice_sport = _get(data, 'title_advice_sport');
  const description_advice_sport = _get(data, 'description_advice_sport');
  const workers_first_level = _get(data, 'workers_first_level');
  const workers_second_level = _get(data, 'workers_second_level');
  const { t } = useTranslation('common');

  const pageTitle = t('aboutUsPageTitle');
  return (
    <div>
      <AboutUsInfoSection title={pageTitle} subtitle={aboutUsDescr} directors={directors} />
      <SportAdvicePeople
        title={title_advice_sport}
        subtitle={description_advice_sport}
        firstLevelWorkers={workers_first_level}
        secondLevelWorkers={workers_second_level}
      />
      <Sectors items={sectors} title={title_sectors} subtitle={description_sectors} />
      <Organogram image={image_organogram} title={title_organogram} subtitle={description_organogram} />
    </div>
  );
};
