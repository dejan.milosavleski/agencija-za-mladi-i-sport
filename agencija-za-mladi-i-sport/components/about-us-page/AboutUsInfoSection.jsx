import React from 'react';
import styled from '@emotion/styled';
import _map from 'lodash/map';

// Components
import Container from 'react-bootstrap/Container';
import { TitleArea } from '../common/TitleArea';
import { ImageWithText } from '../common/ImageWithText';

const StyledContainer = styled(Container)`
  margin-top: 30px;
  margin-bottom: 30px;
`;

export const AboutUsInfoSection = ({ title, subtitle, directors }) => {
  return (
    <StyledContainer>
      <TitleArea title={title} subtitle={subtitle} />
      <div>
        {_map(directors, (director) => (
          <ImageWithText
            imageUrl={director.image}
            title={director.name_surname}
            subtitle={director.position}
            text={director.text}
          />
        ))}
      </div>
    </StyledContainer>
  );
};
