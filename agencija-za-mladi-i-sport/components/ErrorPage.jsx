import React from 'react';
import Container from 'react-bootstrap/Container';
import styled from '@emotion/styled';
import { breakpoints, theme } from '../styles/theme';
import { useTranslation } from 'next-i18next';

const StyledContainer = styled(Container)`
  display: flex;
  flex-direction: row;
`;

const Flex = styled.div`
  flex: ${(props) => props.flex};
  justify-content: center;
  display: inline-flex;
  padding-top: 30vh;
`;

const SpacingFlex = styled.div`
  flex: 2;
`;

const Code = styled.div`
  font-size: 30px;
  @media ${breakpoints.sm} {
    font-size: 20px;
  }
  font-family: StobiSans-Bold, sans-serif;
  color: ${theme.palette.primaryRed};
`;

const Text = styled.div`
  font-size: 30px;
  @media ${breakpoints.sm} {
    font-size: 20px;
  }
  font-family: StobiSans-Bold, sans-serif;
  color: ${theme.palette.navGray};
  padding-left: 10px;
  text-align: center;
`;

export const ErrorPage = ({ code, message }) => {
  return (
    <StyledContainer>
      <SpacingFlex />
      <Flex flex={3}>
        <Code>{code}</Code>
        <Text>{message}</Text>
      </Flex>
      <SpacingFlex />
    </StyledContainer>
  );
};
