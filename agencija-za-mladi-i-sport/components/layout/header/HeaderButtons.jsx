import React from 'react';
import Image from 'next/image';
import Dropdown from 'react-bootstrap/Dropdown';
import macedoniaFlag from '../../../assets/icons/macedonia-flag.svg';
import albaniaFlag from '../../../assets/icons/albanian-flag.svg';
import gbFlag from '../../../assets/icons/great-britain-flag.svg';
import { navigateTo } from '../../../utils';
import youtube from '../../../assets/icons/Youtube.svg';
import facebook from '../../../assets/icons/Facebook.svg';
import styled from '@emotion/styled';
import { breakpoints, theme } from '../../../styles/theme';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import DropdownButton from 'react-bootstrap/DropdownButton';
import { locales } from '../../../contexts/LocaleContext';
import NavDropdown from 'react-bootstrap/NavDropdown';
import dropdownArrow from '../../../assets/icons/arrow-gray.svg';

const FacebookButton = styled.div`
  margin-left: 15px;
  align-self: center;
  :hover {
    cursor: pointer;
  }
`;

const YoutubeButton = styled.div`
  align-self: center;
  :hover {
    cursor: pointer;
  }
`;

const Country = styled.div`
  display: inline-flex;
  height: 18px;
`;

const Abbreviation = styled.div`
  padding-left: 10px;
`;

const MediaButtons = styled(ButtonGroup)`
  margin-top: 10px;
  padding-right: 20px;
  justify-content: space-between !important;
  max-width: 170px;
  width: 100%;
  @media ${breakpoints.md} {
    padding-right: 0;
    max-width: 260px;
  }
`;

const StyledDropdownButton = styled(DropdownButton)`
  width: 45px;
  z-index: 3;

  button:after {
    display: none;
  }

  button:focus {
    box-shadow: none;
  }

  button:focus-visible {
    box-shadow: none;
  }

  .dropdown-menu {
    > a {
      color: black;
    }
    top: -12px !important;
    background-color: white;
  }

  .dropdown-menu:hover {
    > a {
      color: black;
      background-color: white;
    }
  }
`;

export const StyledNavDropdownContainer = styled.div`
  display: inline-flex;
`;

export const SocialMediaButtons = styled.div`
  display: inline-flex;
`;

export const StyledNavDropdownItem = styled(NavDropdown)`
  width: 100%;
  z-index: 3;
`;

export const ArrowContainer = styled.div`
  padding-top: 7px;
  right: 8px;
  position: relative;
`;

export const HeaderButtons = ({ setActiveLocale, titleIcon, social_media, isSmall }) => {
  const mkdLabel = 'MKD';
  const sqLabel = 'SQ';
  const engLabel = 'ENG';

  return (
    <MediaButtons>
      <StyledNavDropdownContainer>
        <StyledDropdownButton variant="outlined-secondary" title={<Image src={titleIcon} alt="flag" />}>
          <Dropdown.Item eventKey="1" onClick={() => setActiveLocale(locales.MK)}>
            <Country>
              <Image src={macedoniaFlag} alt="mkdflag" width={20} height={20} />
              <Abbreviation>{mkdLabel}</Abbreviation>
            </Country>
          </Dropdown.Item>
          <Dropdown.Item eventKey="2" onClick={() => setActiveLocale(locales.SQ)}>
            <Country>
              <Image src={albaniaFlag} alt="sqflag" width={20} height={15} />
              <Abbreviation>{sqLabel}</Abbreviation>
            </Country>
          </Dropdown.Item>
          <Dropdown.Item eventKey="3" onClick={() => setActiveLocale(locales.EN)}>
            <Country>
              <Image src={gbFlag} alt="gbflag" width={20} height={15} />
              <Abbreviation>{engLabel}</Abbreviation>
            </Country>
          </Dropdown.Item>
        </StyledDropdownButton>
        <ArrowContainer>
          <Image src={dropdownArrow} width={10} height={10} alt="arrow" />
        </ArrowContainer>
      </StyledNavDropdownContainer>
      {isSmall ? (
        <>
          {' '}
          <YoutubeButton onClick={() => navigateTo(social_media.youtube)}>
            <Image src={youtube} alt="youtube" width={20} height={20} />
          </YoutubeButton>
          <FacebookButton onClick={() => navigateTo(social_media.facebook)}>
            <Image src={facebook} alt="facebook" width={20} height={20} className="icon" />
          </FacebookButton>
        </>
      ) : (
        <SocialMediaButtons>
          <YoutubeButton onClick={() => navigateTo(social_media.youtube)}>
            <Image src={youtube} alt="youtube" width={20} height={20} />
          </YoutubeButton>
          <FacebookButton onClick={() => navigateTo(social_media.facebook)}>
            <Image src={facebook} alt="facebook" width={20} height={20} className="icon" />
          </FacebookButton>
        </SocialMediaButtons>
      )}
    </MediaButtons>
  );
};
