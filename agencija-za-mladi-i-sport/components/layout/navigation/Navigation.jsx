import React from 'react';
import { breakpoints, theme } from '../../../styles/theme';
import styled from '@emotion/styled';
import _map from 'lodash/map';
// Components
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { LayoutDataContext } from '../../../contexts/LayoutDataContext';
import { LocaleContext } from '../../../contexts/LocaleContext';
import { useMediaQuery } from 'react-responsive';
import { HoverDropdown } from './HoverDropdown';
import { RegularDropdown } from './ClickDropdown';

const NavigationContainer = styled(Navbar)`
  background-color: ${theme.palette.primaryRed};
  min-height: 58px;
  font-size: 18px;
  font-family: StobiSans-Medium, sans-serif;
  .nav-link {
    color: white !important;
  }
  .dropdown-toggle::after {
    vertical-align: 1px;
    border: none;
  }
  .me-auto {
    width: 100%;
    justify-content: space-between;
  }

  .dropdown-menu {
    top: 34px;
    border: none;
  }

  @media ${breakpoints.xl} {
    font-size: 16px;
  }
`;

export const StyledNavDropdownContainer = styled.div`
  display: inline-flex;
`;

export const StyledNavDropdownItem = styled(NavDropdown)`
  width: 100%;
  z-index: 3;
`;

export const ArrowContainer = styled.div`
  padding-top: 9px;
  right: 8px;
  position: relative;
`;

const StyledContainer = styled(Container)`
  padding-right: 13px;
`;

export const Navigation = () => {
  const { headerData } = React.useContext(LayoutDataContext);
  const { locale } = React.useContext(LocaleContext);
  const { primary_menu } = headerData;

  const isSmall = useMediaQuery({
    query: breakpoints.lg
  });
  const [showMap, setShowMap] = React.useState(false);

  const DropdownComponent = isSmall ? RegularDropdown : HoverDropdown;

  return (
    <NavigationContainer expand="lg">
      <StyledContainer>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            {_map(primary_menu, (pm) => {
              const { has_submenu, post_name, post_title, submenu, ID } = pm;
              return has_submenu ? (
                <DropdownComponent
                  key={post_name}
                  primaryMenu={primary_menu}
                  submenu={submenu}
                  title={post_title}
                  id={ID}
                  locale={locale}
                  location={post_name}
                  showMap={showMap}
                  setShowMap={setShowMap}
                />
              ) : (
                <Nav.Link key={post_name} href={`/${locale}/${post_name}`} locale={locale}>
                  {post_title}
                </Nav.Link>
              );
            })}
          </Nav>
        </Navbar.Collapse>
      </StyledContainer>
    </NavigationContainer>
  );
};
