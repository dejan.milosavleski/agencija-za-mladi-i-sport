import React from 'react';
import { goTo } from '../../../utils';
import _map from 'lodash/map';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Image from 'next/image';
import dropdownArrow from '../../../assets/icons/dropdown-arrow.svg';
import _reduce from 'lodash/reduce';
import { ArrowContainer, StyledNavDropdownContainer, StyledNavDropdownItem } from './Navigation';

const ONCE = [];

export const HoverDropdown = ({ primaryMenu, showMap, setShowMap, location, title, id, submenu, locale }) => {
  const initiallyClosed = _reduce(
    primaryMenu,
    (acc, pm) =>
      Object.assign(acc, {
        [pm.ID]: false
      }),
    {}
  );

  React.useEffect(() => {
    setShowMap(initiallyClosed);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, ONCE);

  const openDropdown = React.useCallback(
    (itemId) => {
      const updatedMap = { ...initiallyClosed, [itemId]: true };
      setShowMap(updatedMap);
    },
    [initiallyClosed, setShowMap]
  );

  const closeAll = React.useCallback(() => {
    setShowMap(initiallyClosed);
  }, [initiallyClosed, setShowMap]);

  return (
    <StyledNavDropdownContainer
      onMouseEnter={() => openDropdown(id)}
      onMouseLeave={closeAll}
      onClick={() => goTo(`/${locale}/${location}`)}
    >
      <StyledNavDropdownItem title={title} key={id} id={id} show={showMap[id]}>
        {_map(submenu, (sm) => (
          <NavDropdown.Item key={sm.ID} id={sm.ID} href={`/${locale}/${location}/${sm.post_name}`}>
            {sm.post_title}
          </NavDropdown.Item>
        ))}
      </StyledNavDropdownItem>
      <ArrowContainer>
        <Image src={dropdownArrow} width={12} height={12} alt="arrow" />
      </ArrowContainer>
    </StyledNavDropdownContainer>
  );
};
