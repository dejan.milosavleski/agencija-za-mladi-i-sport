import React from 'react';
import _map from 'lodash/map';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Image from 'next/image';
import dropdownArrow from '../../../assets/icons/dropdown-arrow.svg';
import { ArrowContainer, StyledNavDropdownContainer, StyledNavDropdownItem } from './Navigation';

export const RegularDropdown = ({ location, title, id, submenu, locale }) => {
  return (
    <StyledNavDropdownContainer>
      <StyledNavDropdownItem title={title} key={id} id={id}>
        {_map(submenu, (sm) => (
          <NavDropdown.Item id={sm.ID} href={`/${locale}/${location}/${sm.post_name}`}>
            {sm.post_title}
          </NavDropdown.Item>
        ))}
      </StyledNavDropdownItem>
      <ArrowContainer>
        <Image src={dropdownArrow} width={12} height={12} alt="arrow" />
      </ArrowContainer>
    </StyledNavDropdownContainer>
  );
};
