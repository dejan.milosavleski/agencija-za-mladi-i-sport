import React from 'react';
import styled from '@emotion/styled';

// Components
import { EmailSubscription } from './EmailSubscription';
import { BottomFooter } from './BottomFooter';

const FooterContainer = styled.div`
  margin-top: auto;
`;

export const Footer = () => {
  return (
    <FooterContainer>
      <EmailSubscription />
      <BottomFooter />
    </FooterContainer>
  );
};
