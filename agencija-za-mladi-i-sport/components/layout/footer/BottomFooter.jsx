import React from 'react';
import { breakpoints, theme } from '../../../styles/theme';
import styled from '@emotion/styled';
import _get from 'lodash/get';

// Components
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'next/image';
import Link from 'next/link';

//  Icons and images
import amsImage from '../../../assets/icons/logoAMS.svg';
import oscImage from '../../../assets/icons/OSCE_logo.svg';
import facebook from '../../../assets/icons/awesome-facebook.svg';
import youtube from '../../../assets/icons/awesome-youtube.svg';
import { LayoutDataContext } from '../../../contexts/LayoutDataContext';
import { navigateTo } from '../../../utils';
import { LocaleContext } from '../../../contexts/LocaleContext';
import { useTranslation } from 'next-i18next';

const Content = styled(Container)`
  background-color: ${theme.palette.white};
`;

const ItemsContainer = styled.div`
  display: flex;
  flex-direction: column;
  @media ${breakpoints.md} {
    margin-bottom: 15px;
  }
`;

const SocialContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 10px;
`;

const IconContainer = styled.div`
  margin-left: 20px;
`;

const StyledImage = styled(Image)`
  :hover {
    cursor: pointer;
  }
`;

const TextItem = styled.div`
  font-size: 14px;
  margin-bottom: 4px;
  max-width: 300px;
`;

const LinkEncapsulate = styled.div`
  font-size: 14px;
  margin-bottom: 4px;
`;

const Title = styled.div`
  color: ${theme.palette.primaryRed};
  font-weight: 500;
  font-size: 18px;
  margin-bottom: 10px;
`;

const SpaceDivider = styled.div`
  margin-top: 30px;
`;

const InfoRow = styled(Row)`
  margin-top: 80px;
`;

const RightsRow = styled(Row)`
  border-top: 1px solid #eceff5;
`;

const RightsText = styled.div`
  margin-top: 20px;
  margin-bottom: 20px;
  font-size: 14px;
  text-align: center;
`;

const SupportedLabel = styled.div`
  font-size: 14px;
  margin-bottom: 10px;
`;

const StyledLink = styled.a`
  :hover {
    color: #d8232a;
  }
`;

export const BottomFooter = () => {
  const { footerData } = React.useContext(LayoutDataContext);
  const { sitemap, social_media, useful_links, contact_info } = footerData;
  const { locale } = React.useContext(LocaleContext);
  const { t } = useTranslation();

  const supportedBy = t('supportedBy');
  const allRightsReserved = t('allRightsReserved');

  return (
    <Content>
      <InfoRow>
        <Col xs={6} sm={6} md={3}>
          <Image src={amsImage} alt="ams-logo" width={256} height={103} />
          <SpaceDivider />
          <SupportedLabel>{supportedBy}</SupportedLabel>
          <Image src={oscImage} alt="osc-logo" width={172} height={61} />
        </Col>
        <Col xs={6} sm={6} md={3}>
          <ItemsContainer>
            <Title>{_get(sitemap, 'menu_title')}</Title>
            {_get(sitemap, 'menu_items', []).map((mi) => (
              <LinkEncapsulate key={mi.post_title}>
                <Link href={`/${mi.post_name}`} locale={locale}>
                  {mi.post_title}
                </Link>
              </LinkEncapsulate>
            ))}
          </ItemsContainer>
        </Col>
        <Col xs={6} sm={6} md={3}>
          <ItemsContainer>
            <Title>{_get(contact_info, 'menu_title')}</Title>
            <TextItem>{_get(contact_info, 'address')}</TextItem>
            <SpaceDivider />
            {_get(contact_info, 'phone_numbers', []).map((telephone) => (
              <TextItem key={telephone.phone_number}>{telephone.phone_number}</TextItem>
            ))}
            {_get(contact_info, 'email_addresses', []).map((ea) => (
              <TextItem key={ea.email_address}>{ea.email_address}</TextItem>
            ))}
            <>
              <SpaceDivider />
              <Title>{_get(social_media, 'menu_title')}</Title>
              <SocialContainer>
                <StyledImage
                  src={youtube}
                  alt="youtube"
                  width={30}
                  height={30}
                  onClick={() => navigateTo(social_media.youtube)}
                />
                <IconContainer>
                  <StyledImage
                    src={facebook}
                    alt="facebook"
                    width={25}
                    height={25}
                    className="icon"
                    onClick={() => navigateTo(social_media.facebook)}
                  />
                </IconContainer>
              </SocialContainer>
            </>
          </ItemsContainer>
        </Col>
        <Col xs={6} sm={6} md={3}>
          <ItemsContainer>
            <Title>{_get(useful_links, 'menu_title')}</Title>
            {_get(useful_links, 'menu_items', []).map((mi) => (
              <LinkEncapsulate key={mi.url}>
                <StyledLink target={mi.target} href={mi.url}>
                  {mi.title}
                </StyledLink>
              </LinkEncapsulate>
            ))}
          </ItemsContainer>
        </Col>
      </InfoRow>
      <RightsRow>
        <RightsText>{allRightsReserved}</RightsText>
      </RightsRow>
    </Content>
  );
};
