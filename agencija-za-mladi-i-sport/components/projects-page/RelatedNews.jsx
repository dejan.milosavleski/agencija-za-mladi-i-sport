import React from 'react';
import Row from 'react-bootstrap/Row';
import _map from 'lodash/map';
import _get from 'lodash/get';
import Col from 'react-bootstrap/Col';
import { CardWithText } from '../common/CardWithText';
import Container from 'react-bootstrap/Container';
import { TitleArea } from '../common/TitleArea';
import styled from '@emotion/styled';
import { useRouter } from 'next/router';

const TitleWrapper = styled.div`
  margin: 20px 0;
`;

export const RelatedNews = ({ title, content }) => {
  const router = useRouter();
  const { category } = router;
  return (
    <Container>
      <TitleWrapper>
        <TitleArea title={title} small />
      </TitleWrapper>
      <Row>
        {_map(content, (item) => {
          const { excerpt, post_name, slug, title, thumbnail, imageUrl, ID, categories } = item;
          const currentCategory = category || _get(categories, '[0]');

          return (
            <Col xs={12} md={4} key={ID}>
              <CardWithText
                imageUrl={thumbnail || imageUrl}
                title={title}
                currentCategory={currentCategory}
                subtitle={excerpt}
                postName={post_name || slug}
                white
                height={14}
              />
            </Col>
          );
        })}
      </Row>
    </Container>
  );
};
