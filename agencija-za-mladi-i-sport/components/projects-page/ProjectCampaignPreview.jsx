import React from 'react';
import Row from 'react-bootstrap/Row';
import styled from '@emotion/styled';
import Col from 'react-bootstrap/Col';
import Carousel from 'react-bootstrap/Carousel';
import _map from 'lodash/map';
import _isEmpty from 'lodash/isEmpty';
import { goTo } from '../../utils';
import { useRouter } from 'next/router';
import { StyledButton } from '../common/StyledButton';
import { breakpoints } from '../../styles/theme';
import Link from 'next/link';
import { LocaleContext } from '../../contexts/LocaleContext';
import { useTranslation } from 'next-i18next';

const RightCol = styled(Col)`
  align-self: center;
  margin-left: 2vw;
  @media ${breakpoints.md} {
    margin-left: 0;
  }
`;

const StyledRow = styled(Row)`
  padding-right: 20px;
`;

const Title = styled.div`
  font-size: 2.2vw;
  font-family: StobiSans-Medium, sans-serif;
  @media ${breakpoints.lg} {
    font-size: 2.5vw;
  }
  @media ${breakpoints.md} {
    font-size: 4.95vw;
  }
`;

const Text = styled.div`
  padding-top: 10px;
  padding-bottom: 10px;
  font-size: 0.95vw;
  @media ${breakpoints.xl} {
    font-size: 1.5vw;
  }
  @media ${breakpoints.md} {
    font-size: 2.95vw;
  }
`;

const CarouselItem = styled(Carousel.Item)`
  width: 100%;
  text-align: left;
`;

const StyledImage = styled.img`
  width: 100%;
  height: 23vw;

  :hover {
    cursor: ${(props) => (props.campaign === 'campaign' ? 'pointer' : 'normal')};
  }

  @media ${breakpoints.lg} {
    height: 23vw;
  }
  @media ${breakpoints.md} {
    height: 40vw;
  }
`;

const MainRow = styled(Row)`
  margin-bottom: 20px;
`;

export const ProjectCampaignPreview = ({ projectImages, title, text, postName, imageUrl, campaign }) => {
  const { t } = useTranslation('common');

  const buttonLabel = t('findOutMore');

  const { locale } = React.useContext(LocaleContext);
  const router = useRouter();
  const { pathname } = router;

  const handleImageClick = () => {
    if (campaign) {
      goTo(`${locale}${pathname}/${postName}`);
    }
  };

  return (
    <MainRow>
      <Col sm={12} md={6}>
        {!_isEmpty(projectImages) ? (
          <Carousel nextIcon={''} prevIcon={''}>
            {_map(projectImages, (si) => (
              <CarouselItem>
                <StyledImage
                  src={si}
                  alt="sliderImage"
                  campaign={campaign ? 'campaign' : ''}
                  onClick={handleImageClick}
                />
              </CarouselItem>
            ))}
          </Carousel>
        ) : (
          <StyledImage
            src={imageUrl}
            alt="sliderImage"
            campaign={campaign ? 'campaign' : ''}
            onClick={handleImageClick}
          />
        )}
      </Col>
      <RightCol sm={12} md={5}>
        <StyledRow>
          <Title>
            <Link href={`${pathname}/${postName}`}>{title}</Link>
          </Title>
        </StyledRow>
        <StyledRow>
          <Text>{text}</Text>
        </StyledRow>
        <StyledRow>
          <StyledButton minwidth={150} marginleft={15} onClick={() => goTo(`/${locale}${pathname}/${postName}`)}>
            {buttonLabel}
          </StyledButton>
        </StyledRow>
      </RightCol>
    </MainRow>
  );
};
