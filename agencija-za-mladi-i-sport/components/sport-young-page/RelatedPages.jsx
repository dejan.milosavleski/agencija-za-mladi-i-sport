import React from 'react';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import styled from '@emotion/styled';
import _map from 'lodash/map';
import _toUpper from 'lodash/toUpper';
import { breakpoints, theme } from '../../styles/theme';
import Link from 'next/link';

const RelatedPage = styled.div`
  min-height: 68px;
  background-color: ${theme.palette.lightGray};
  margin-bottom: 20px;
  text-align: center;
  padding-top: 23px;
  :hover {
    cursor: pointer;
    background-color: ${theme.palette.primaryRed};
    color: white;
  }
`;

const StyledContainer = styled(Container)`
  margin-top: 6vw;
  margin-bottom: 5vw;
`;

const StyledCol = styled(Col)`
  @media ${breakpoints.sm} {
    text-align: -webkit-center;
  }
`;

export const RelatedPages = ({ relatedPages, locale }) => {
  return (
    <StyledContainer>
      <Row>
        {_map(relatedPages, (rp) => (
          <StyledCol xs={12} sm={6} md={3} lg={3}>
            <Link href={rp.post_name} locale={locale}>
              <RelatedPage>{_toUpper(rp.title)}</RelatedPage>
            </Link>
          </StyledCol>
        ))}
      </Row>
    </StyledContainer>
  );
};
