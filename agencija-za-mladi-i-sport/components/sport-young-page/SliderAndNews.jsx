import React from 'react';
import Row from 'react-bootstrap/Row';
import Carousel from 'react-bootstrap/Carousel';
import _map from 'lodash/map';
import styled from '@emotion/styled';
import { breakpoints } from '../../styles/theme';
import Link from 'next/link';
import { PromotedPublicCallsContent } from '../common/public-calls-and-promotions/PromotedPublicCallsContent';
import { useTranslation } from 'next-i18next';
import Col from 'react-bootstrap/Col';
import { CarouselCol } from '../common/StyledButton';
import { SliderSubtitle, SliderTitle } from '../common/StyledTypography';
import { PlaceholderImage } from '../common/PlaceholderImage';

export const StyledImage = styled.div`
  height: 23vw;
  width: 46vw;
  background: ${(props) => `linear-gradient(180deg, #00000000 0%, #000000 100%),
    url(${props.url}) no-repeat center`};
  background-size: cover;
  @media ${breakpoints.xl} {
    height: 30vw;
    width: 60vw;
  }

  @media ${breakpoints.xl} {
    height: 35vw;
    width: 70vw;
  }

  @media ${breakpoints.lg} {
    height: 45vw;
    width: 90vw;
  }
`;

const StyledRow = styled(Row)`
  margin: 20px 0;
`;

export const SliderAndNews = ({ sliderInfo, news, locale }) => {
  const { t } = useTranslation('common');
  const title = t('publicCallsTitle');
  const seeAll = t('seeAll');

  return (
    <StyledRow>
      <CarouselCol md={12} lg={7}>
        <Carousel interval={3000}>
          {_map(sliderInfo, (si) => (
            <Carousel.Item>
              {si.thumbnail ? (
                <StyledImage url={si.thumbnail} alt="sliderImage" />
              ) : (
                <PlaceholderImage height="23vw" width="46vw" />
              )}
              <Carousel.Caption>
                <SliderTitle>
                  <Link href={`/public-call/${si.post_name}`} locale={locale}>
                    {si.title}
                  </Link>
                </SliderTitle>
                <SliderSubtitle>{si.excerpt}</SliderSubtitle>
              </Carousel.Caption>
            </Carousel.Item>
          ))}
        </Carousel>
      </CarouselCol>
      <Col md={12} lg={5}>
        <PromotedPublicCallsContent promotedPublicCalls={news} title={title} seeAll={seeAll} page="public-call" />
      </Col>
    </StyledRow>
  );
};
