import React from 'react';
import { TitleArea } from '../common/TitleArea';
import Container from 'react-bootstrap/Container';
import { SliderAndNews } from './SliderAndNews';
import styled from '@emotion/styled';
import { RelatedPages } from './RelatedPages';
import { InfoBanner } from '../front-page/InfoBanner';
import { RelatedNews } from '../projects-page/RelatedNews';
import { StyledButton } from '../common/StyledButton';
import { goTo } from '../../utils';
import Row from 'react-bootstrap/Row';
import { useTranslation } from 'next-i18next';
import { ProjectsSection } from '../front-page/projects/Projects';
import { useRouter } from 'next/router';

const PageContainer = styled(Container)`
  padding-top: 30px;
  padding-bottom: 30px;
`;

const StyledRow = styled(Row)`
  justify-content: space-evenly;
`;

export const SportYoungPage = ({ pageTitle, content, category, relatedNews }) => {
  const { t } = useTranslation('common');

  const router = useRouter();
  const { locale } = router;

  const allNewsButtonLabel = t('allNews');
  const relatedNewsLabel = t('relatedNews');
  const { projects, promo_banner, promo_content, promoted_public_calls, related_pages } = content;

  return (
    <>
      <TitleArea title={pageTitle} />
      <PageContainer>
        <SliderAndNews sliderInfo={promo_content} news={promoted_public_calls} category={category} locale={locale} />
        <RelatedPages relatedPages={related_pages} locale={locale} />
        <InfoBanner contentData={promo_banner} />
        <ProjectsSection projects={projects} />
        <RelatedNews content={relatedNews} title={relatedNewsLabel} />
        <StyledRow>
          <StyledButton minwidth={190} onClick={() => goTo(`/${locale}/news/${category}`)}>
            {allNewsButtonLabel}
          </StyledButton>
        </StyledRow>
      </PageContainer>
    </>
  );
};
