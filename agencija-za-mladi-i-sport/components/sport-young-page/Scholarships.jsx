import React from 'react';
import { NewsItemList } from '../common/NewsItemList';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import styled from '@emotion/styled';
import Image from 'react-bootstrap/Image';
import { useMediaQuery } from 'react-responsive';
import { breakpoints } from '../../styles/theme';
import { useTranslation } from 'next-i18next';

const StyledCol = styled(Col)`
  margin: 50px 0;
`;

export const Scholarships = ({ items, imageUrl }) => {
  const { t } = useTranslation('common');

  const seeAllLabel = t('seeAll');
  const listTitle = t('scholarships');
  const isSmall = useMediaQuery({
    query: breakpoints.md
  });

  return (
    <Container>
      <Row>
        <StyledCol xs={12} md={6}>
          {isSmall ? (
            <Image src={imageUrl} fluid />
          ) : (
            <NewsItemList
              title={listTitle}
              items={items}
              endItem={{ text: seeAllLabel, href: '/public-calls/scolarships' }}
              alignCenter
              paddingLeft={50}
            />
          )}
        </StyledCol>
        <StyledCol xs={12} md={6}>
          {isSmall ? (
            <NewsItemList
              title={listTitle}
              items={items}
              endItem={{ text: seeAllLabel, href: '/public-calls/scolarships' }}
              alignCenter
              paddingLeft={50}
            />
          ) : (
            <Image src={imageUrl} fluid />
          )}
        </StyledCol>
      </Row>
    </Container>
  );
};
