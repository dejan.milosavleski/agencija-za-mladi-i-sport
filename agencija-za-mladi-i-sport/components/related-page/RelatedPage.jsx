import React from 'react';
import { SingleTitleArea } from '../single-item-page/SingleTitleArea';
import { ImageContainer } from '../single-item-page/SingleItemMainArea';
import { StyledHtml } from '../common/StyledHtml';
import styled from '@emotion/styled';
import Container from 'react-bootstrap/Container';

const PageContainer = styled(Container)`
  padding: 30px 0;
`;

export const RelatedPage = ({ data }) => {
  const page_title = 'Related page dummy title';
  const imageUrl = 'Dummy page title';
  const content = 'dangerouslySetInnerHTML content here';

  return (
    <PageContainer>
      <SingleTitleArea title={page_title} />
      <ImageContainer bottom={50}>
        <img src={imageUrl} alt="picture" />
      </ImageContainer>
      <StyledHtml dangerouslySetInnerHTML={{ __html: content }} width="100%" />
    </PageContainer>
  );
};
