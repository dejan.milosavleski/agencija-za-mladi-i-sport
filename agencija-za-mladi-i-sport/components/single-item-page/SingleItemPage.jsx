import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import styled from '@emotion/styled';
import { theme } from '../../styles/theme';
import _map from 'lodash/map';
import _filter from 'lodash/filter';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { getLevelPath } from '../../utils';
import { PageTitle } from '../common/StyledTypography';
import { SingleItemMainArea } from './SingleItemMainArea';

const ImageContainer = styled.div`
  width: 100%;
  margin-bottom: ${(props) => props.bottom}px;
`;

const PageContainer = styled(Container)`
  margin-top: 30px;
  margin-bottom: 30px;
`;

const Title = styled.div`
  color: ${theme.palette.navGray};
  font-size: 22px;
  font-family: StobiSans-Medium, sans-serif;
`;

const ItemTitle = styled.div`
  color: ${theme.palette.navGray};
  font-size: 22px;
  font-family: StobiSans-Bold, sans-serif;
  margin-bottom: 20px;
`;

const Divider = styled.hr`
  border: 3px solid ${theme.palette.primaryRed};
  width: 60px;
  opacity: 1;
`;

const SmallImage = styled.img`
  max-width: 300px;
`;

const StyledCol = styled(Col)`
  padding-right: 50px;
`;

export const SingleItemPage = ({ title, item, rightSectionTitle, rightSectionItems }) => {
  const { page_title, imageUrl, content } = item;
  const withFilteredCurrent = _filter(rightSectionItems, (ri) => ri.slug !== item.slug);
  const { pathname } = useRouter();
  const path = getLevelPath(pathname, 1);

  return (
    <PageContainer>
      <PageTitle>{title}</PageTitle>
      <Row>
        <StyledCol xs={12} md={9}>
          <SingleItemMainArea pageTitle={page_title} imageUrl={imageUrl} content={content} />
        </StyledCol>
        <Col xs={12} md={3}>
          <Title>{rightSectionTitle}</Title>
          <Divider />
          {_map(withFilteredCurrent, (filteredItem) => (
            <>
              <ImageContainer bottom={20}>
                <SmallImage src={filteredItem.imageUrl} />
              </ImageContainer>
              <ItemTitle>
                <Link href={`/${path}/${filteredItem.slug}`}>{filteredItem.title}</Link>
              </ItemTitle>
            </>
          ))}
        </Col>
      </Row>
    </PageContainer>
  );
};
