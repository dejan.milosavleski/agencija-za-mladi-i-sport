import React from 'react';
import styled from '@emotion/styled';
import { theme } from '../../styles/theme';

const Divider = styled.div`
  border: 1px solid ${theme.palette.mediumGray};
  width: 60px;
  opacity: 1;
`;

const DividerContainer = styled.div`
  text-align: left;
  margin-left: 30px;
  margin-bottom: 30px;
  margin-top: 10px;
`;

const Title = styled.div`
  font-size: 36px;
  color: ${theme.palette.navGray};
  font-family: StobiSans-Bold, sans-serif;
`;

const TextContainer = styled.div`
  text-align: left;
`;

export const SingleTitleArea = ({ title }) => {
  return (
    <TextContainer>
      <Title>{title}</Title>
      <DividerContainer>
        <Divider />
      </DividerContainer>
    </TextContainer>
  );
};
