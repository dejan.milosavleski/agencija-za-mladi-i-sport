import React from 'react';
import styled from '@emotion/styled';

// Components
import mapboxgl from '!mapbox-gl';

// Constants
import { mapAccessToken } from '../../contexts/constants';

mapboxgl.accessToken = mapAccessToken;

const MapContainer = styled.div`
  height: 400px;
`;

export const Map = ({ longitude, latitude }) => {
  const mapContainer = React.useRef(null);
  const map = React.useRef(null);
  const [lng, setLng] = React.useState(longitude);
  const [lat, setLat] = React.useState(latitude);
  const [zoom, setZoom] = React.useState(14);

  React.useEffect(() => {
    if (map.current) {
      // TODO smihail 2021-09-26: fix marker doesn't show up
      var marker = new mapboxgl.Marker().setLngLat([longitude, latitude]).addTo(map);

      return;
    } // initialize map only once
    map.current = new mapboxgl.Map({
      container: mapContainer.current,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [lng, lat],
      zoom: zoom
    });
  }, [lat, latitude, lng, longitude, zoom]);

  return (
    <div>
      <MapContainer ref={mapContainer} />
    </div>
  );
};
