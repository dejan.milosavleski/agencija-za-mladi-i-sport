import React from 'react';
import styled from '@emotion/styled';
import { theme } from '../../styles/theme';

// Components
import Image from 'next/image';

// Icons
import email from '../../assets/icons/red-email.svg';
import telephone from '../../assets/icons/phone-ring.svg';
import location from '../../assets/icons/red-location.svg';

const Text = styled.div`
  display: inline-flex;
  font-size: 18px;
  font-family: StobiSans-Light, sans-serif;
  line-height: 36px;
`;

const Icon = styled.div`
  margin-right: 8px;
  margin-top: 1px;
`;

const Content = styled.div`
  margin: 10px 0;
  display: flex;
  flex-direction: column;
`;

const TitleSection = styled.div`
  border-bottom: 1px solid ${theme.palette.primaryRed};
  margin-bottom: 10px;
  max-width: 395px;
  font-family: StobiSans-Medium, sans-serif;
  font-size: 24px;
`;

export const ContactPageInfoSection = ({ item }) => {
  const { address, email_address, fax_number, name_surname, number, title_section } = item;
  return (
    <Content>
      <TitleSection>{title_section}</TitleSection>
      {name_surname && <Text>{name_surname}</Text>}
      {email_address && (
        <Text>
          <Icon>
            <Image src={email} alt="email" width={15} height={15} />
          </Icon>
          {email_address}
        </Text>
      )}
      {address && (
        <Text>
          <Icon>
            <Image src={location} alt="phone" width={15} height={15} />
          </Icon>
          {address}
        </Text>
      )}
      {(number || fax_number) && (
        <Text>
          <Icon>
            <Image src={telephone} alt="phone" width={15} height={15} />
          </Icon>
          <div>
            <div>{number}</div>
            <div>{fax_number}</div>
          </div>
        </Text>
      )}
    </Content>
  );
};
