import React from 'react';
import styled from '@emotion/styled';
import { breakpoints, theme } from '../../styles/theme';

// Components
import Image from 'next/image';
import Container from 'react-bootstrap/Container';

// icons
import mLogo from '../../assets/images/background-m-logo.png';

const StyledContainer = styled(Container)`
  margin-bottom: 40px;
  padding: 30px 0;
  background-color: ${theme.palette.mediumGray};
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  @media ${breakpoints.md} {
    width: 100%;
    height: 30vw;
  }
`;

const StyledImage = styled.div``;

export const PlaceholderImage = ({ height, width }) => {
  return (
    <StyledContainer height={height} width={width}>
      <StyledImage>
        <Image src={mLogo} alt="mlogo" width={395} height={109} />
      </StyledImage>
    </StyledContainer>
  );
};
