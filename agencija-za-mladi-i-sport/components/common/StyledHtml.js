import styled from '@emotion/styled';

export const StyledHtml = styled.div`
  font-size: 16px;
  img {
    display: block;
    margin: 15px 0;
    width: ${(props) => props.width};
  }
`;

export const Flex = styled.div`
  flex: ${(props) => props.flex};
  line-height: 1.5em;
  height: 3em;
  overflow: hidden;
  text-overflow: ellipsis;
  padding-right: 2px;
  padding-top: ${(props) => props.paddingtop}px;
`;
