import React from 'react';
import styled from '@emotion/styled';
import _isNil from 'lodash/isNil';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';

// Components
import Link from 'next/link';
import Image from 'next/image';
import ListGroup from 'react-bootstrap/ListGroup';
import { LinkContainer, StyledItem } from './Categories';

// icons
import arrowUp from '../../../assets/icons/arrow-up.png';
import arrowDown from '../../../assets/icons/arrow-down.png';
import { SubcategoryTreeItem } from './SubcategoryTreeItem';

export const SubLinkContainer = styled.div`
  opacity: ${(props) => (props.itemSelected ? '100%' : '54%')};
  height: 16px;
  :hover {
    opacity: 100%;
  }
  margin-left: ${(props) => (props.ternary ? 20 : 10)}px;
`;

export const SubCategoryItem = styled(ListGroup.Item)`
  min-height: 40px;
  display: inline-flex;
  justify-content: space-between;
  font-size: 18px;
  font-family: StobiSans-Medium, sans-serif;
`;

export const Icon = styled.div`
  :hover {
    cursor: pointer;
  }
`;

export const CategoryTreeItem = ({
  category,
  categoryActive,
  firstPageName,
  locale,
  subcategories,
  currentSubcategory
}) => {
  const [showSubTree, setShowSubTree] = React.useState(null);

  React.useEffect(() => {
    if (_isNil(showSubTree)) {
      setShowSubTree(categoryActive);
    }
  }, [categoryActive, showSubTree]);

  const arrowIcon = showSubTree ? arrowUp : arrowDown;

  const iconSize = showSubTree ? 17 : 20;
  const categoryUrl = `/${firstPageName}/${category.slug}`;

  return (
    <>
      <StyledItem key={category.term_id}>
        <LinkContainer itemSelected={categoryActive}>
          <Link href={categoryUrl} locale={locale}>
            {category.name}
          </Link>
        </LinkContainer>
        <Icon onClick={() => setShowSubTree(!showSubTree)}>
          <Image src={arrowIcon} alt="left" width={iconSize} height={iconSize} />
        </Icon>
      </StyledItem>
      {showSubTree &&
        subcategories.map((subcategory) => {
          const children = _get(subcategory, 'children', []);
          const subcategoryItemSelected = currentSubcategory === subcategory.slug;
          return _isEmpty(children) ? (
            <SubCategoryItem key={subcategory.term_id}>
              <SubLinkContainer itemSelected={subcategoryItemSelected}>
                <Link href={`${categoryUrl}/${subcategory.slug}`} locale={locale}>
                  {subcategory.name}
                </Link>
              </SubLinkContainer>
            </SubCategoryItem>
          ) : (
            <SubcategoryTreeItem
              itemSelected={subcategoryItemSelected}
              subcategory={subcategory}
              subcategoryUrl={`${categoryUrl}/${subcategory.slug}`}
              currentSubcategory={currentSubcategory}
              locale={locale}
              items={children}
            />
          );
        })}
    </>
  );
};
