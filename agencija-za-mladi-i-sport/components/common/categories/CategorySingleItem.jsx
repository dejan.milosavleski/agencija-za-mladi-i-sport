// @flow
import React from 'react';

// Components
import Link from 'next/link';
import { LinkContainer, StyledItem } from './Categories';

export const CategorySingleItem = ({ category, categoryActive, firstPageName, locale }) => {
  return (
    <StyledItem key={category.term_id}>
      <LinkContainer itemSelected={categoryActive}>
        <Link href={`/${firstPageName}/${category.slug}`} locale={locale}>
          {category.name}
        </Link>
      </LinkContainer>
    </StyledItem>
  );
};
