import React from 'react';
import styled from '@emotion/styled';
import { useTranslation } from 'next-i18next';
import _map from 'lodash/map';
import _isEmpty from 'lodash/isEmpty';
import { breakpoints, theme } from '../../../styles/theme';

// Components
import ListGroup from 'react-bootstrap/ListGroup';
import { CategoryTreeItem } from './CategoryTreeItem';
import { CategorySingleItem } from './CategorySingleItem';

const Content = styled.div`
  min-height: 23vw;
  max-width: 380px;
  position: sticky;
  top: 0;
  @media ${breakpoints.xl} {
    max-width: 500px;
  }
  @media ${breakpoints.md} {
    margin-bottom: 15px;
  }
`;

const Title = styled.div`
  color: ${theme.palette.navGray};
  font-size: 22px;
  font-family: StobiSans-Medium, sans-serif;
`;

const Divider = styled.hr`
  border: 3px solid ${theme.palette.primaryRed};
  width: 60px;
  opacity: 1;
`;

export const StyledItem = styled(ListGroup.Item)`
  min-height: 50px;
  display: ${(props) => (props.first ? '' : 'inline-flex')};
  justify-content: ${(props) => (props.first ? 'none' : 'space-between')};
  font-size: 22px;
  font-family: StobiSans-Bold, sans-serif;
`;

export const LinkContainer = styled.div`
  opacity: ${(props) => (props.itemSelected ? '100%' : '54%')};
  height: 22px;
  :hover {
    opacity: 100%;
  }
`;

export const StyledList = styled(ListGroup)`
  padding: 10px;
`;

export const Categories = ({ categories, currentCategory, firstPageName, locale, currentSubcategory }) => {
  const { t } = useTranslation('common');
  const title = t('categoriesLabel');

  return (
    <Content>
      <StyledList variant="flush">
        <StyledItem first="first" itemSelected>
          <Title>{title}</Title>
          <Divider />
        </StyledItem>
        {_map(categories, (category) => {
          const subcategories = category.children;
          const categoryActive = category.slug === currentCategory;

          return !_isEmpty(subcategories) ? (
            <CategoryTreeItem
              category={category}
              categoryActive={categoryActive}
              firstPageName={firstPageName}
              locale={locale}
              subcategories={subcategories}
              currentSubcategory={currentSubcategory || {}}
            />
          ) : (
            <CategorySingleItem
              locale={locale}
              firstPageName={firstPageName}
              categoryActive={categoryActive}
              category={category}
            />
          );
        })}
      </StyledList>
    </Content>
  );
};
