// Style
import styled from '@emotion/styled';
import { breakpoints, theme } from '../../styles/theme';

// Components
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

export const StyledButton = styled(Button)`
  border-radius: 30px;
  height: 37px;
  margin: 10px 0 5px 0;
  background-color: white;
  margin-left: ${(props) => props.marginleft}px;
  min-width: ${(props) => props.minwidth}px;
  color: ${theme.palette.navGray};
  width: 150px;
  border-color: ${theme.palette.primaryRed};
  :hover {
    background-color: ${theme.palette.lightPrimaryRed};
    border-color: #cbbfbf;
  }
  :active {
    background-color: white;
    border-color: ${theme.palette.lightPrimaryRed};
    box-shadow: none;
    color: ${theme.palette.navGray};
  }
  :focus {
    background-color: white;
    border-color: ${theme.palette.lightPrimaryRed};
    box-shadow: none;
    color: ${theme.palette.navGray};
  }

  @media ${breakpoints.lg} {
    font-size: 12px;
    width: 115px !important;
    min-width: 115px;
  }
`;

export const StyledIconImage = styled.div`
  display: flex;
  max-width: 10px;
  margin-left: 5px;
  :hover {
    cursor: pointer;
  }
`;

export const CarouselCol = styled(Col)`
  display: flex;
  flex-direction: column;
  align-self: flex-start;
`;
