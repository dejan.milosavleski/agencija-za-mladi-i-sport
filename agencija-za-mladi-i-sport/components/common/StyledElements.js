import styled from '@emotion/styled';
import InfiniteScroll from 'react-infinite-scroll-component';

export const StyledInfiniteScroll = styled(InfiniteScroll)`
  overflow: hidden !important;
`;
