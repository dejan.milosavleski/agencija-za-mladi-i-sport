import React from 'react';
import styled from '@emotion/styled';
import { breakpoints, theme } from '../../styles/theme';

// Components
import Card from 'react-bootstrap/Card';
import Link from 'next/link';

// Context
import { LocaleContext } from '../../contexts/LocaleContext';

// Utils
import { goTo } from '../../utils';

const StyledCard = styled(Card)`
  margin-right: 10px;
  background-color: ${(props) => (props.white ? 'white' : theme.palette.lightGray)};
  border: none;
  width: ${(props) => props.smaller && '60%'};
  margin-top: 20px;
`;

const CardImage = styled(Card.Img)`
  width: 100%;
  height: ${(props) => props.height}vw;
  @media ${breakpoints.lg} {
    height: ${(props) => props.height * 1.2}vw;
  }
  @media ${breakpoints.md} {
    height: ${(props) => props.height * 2}vw;
  }
  @media ${breakpoints.sm} {
    height: ${(props) => props.height * 3}vw;
  }
  :hover {
    cursor: ${(props) => props.click === 'click' && 'pointer'};
  }
`;

const StyledBody = styled(Card.Body)`
  padding-left: 0;
`;

const StyledTitle = styled(Card.Title)`
  @media ${breakpoints.lg} {
    font-size: 2vw;
  }
  @media ${breakpoints.md} {
    font-size: 3vw;
  }
`;

const StyledText = styled(Card.Text)`
  @media ${breakpoints.md} {
    font-size: 2.5vw;
  }
`;

export const CardWithText = ({ imageUrl, title, subtitle, smaller, postName, white, height, currentCategory }) => {
  const { locale } = React.useContext(LocaleContext);

  const handleImageClick = () => {
    if (postName) {
      goTo(`/${locale}/news/${postName}`);
    }
  };

  return (
    <StyledCard smaller={smaller} white={white}>
      <CardImage
        variant="top"
        src={imageUrl}
        onClick={handleImageClick}
        height={height}
        click={postName ? 'click' : ''}
      />
      <StyledBody>
        <StyledTitle>
          {postName ? (
            <Link href={`/news/${currentCategory || ''}/${postName}`} locale={locale}>
              {title}
            </Link>
          ) : (
            title
          )}
        </StyledTitle>
        <StyledText>{subtitle}</StyledText>
      </StyledBody>
    </StyledCard>
  );
};
