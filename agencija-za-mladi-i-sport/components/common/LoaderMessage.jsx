import React from 'react';
import { useTranslation } from 'next-i18next';

export const LoaderMessage = ({ message }) => {
  const { t } = useTranslation('common');
  const displayMessage = message || t('loadingLabel');

  return <h5>{displayMessage}</h5>;
};
