import React from 'react';
import styled from '@emotion/styled';
import { theme } from '../../styles/theme';

const Subtitle = styled.div`
  font-size: 20px;
  color: ${(props) => (props.white ? 'white' : theme.palette.navGray)};
  margin: 20px 0;
`;

const Divider = styled.div`
  border: ${(props) => (props.white ? '1px solid white' : `1px solid ${theme.palette.navGray}`)};
  width: ${(props) => (props.small ? '30px' : '60px')};
  margin-left: ${(props) => props.small && '3.5vw'};
  opacity: 1;
`;

const DividerContainer = styled.div`
  text-align: ${(props) => !props.small && '-webkit-center'};
`;

const Title = styled.div`
  font-size: ${(props) => (props.small ? '22px' : '36px')};
  color: ${(props) => (props.white ? 'white' : theme.palette.primaryRed)};
  font-family: ${(props) => (props.small ? 'StobiSans-Medium, sans-serif' : 'StobiSans-Bold, sans-serif')};
`;

const TextContainer = styled.div`
  text-align: ${(props) => (props.small ? 'left' : 'center')};
  margin: ${(props) => (props.small ? '10px 0' : '50px 0')};
`;

export const TitleArea = ({ title, subtitle, white, small }) => {
  return (
    <TextContainer small={small}>
      <Title white={white} small={small}>
        {title}
      </Title>
      <DividerContainer small={small}>
        <Divider white={white} small={small} />
      </DividerContainer>
      {subtitle && <Subtitle white={white}>{subtitle}</Subtitle>}
    </TextContainer>
  );
};
