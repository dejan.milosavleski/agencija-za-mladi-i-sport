import React from 'react';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import styled from '@emotion/styled';
import { breakpoints, theme } from '../../styles/theme';
import { useTranslation } from 'next-i18next';

const Text = styled.div`
  font-family: StobiSans-Bold, sans-serif;
  padding-top: 20px;
  font-size: 20px;
  @media ${breakpoints.md} {
    font-size: 16px;
  }
`;

const StyledButtons = styled.div`
  width: 287px;
  height: 66px;
  background-color: ${(props) => (props.active ? theme.palette.primaryRed : theme.palette.lightGray)};
  color: ${(props) => (props.active ? 'white' : theme.palette.primaryGray)};

  :hover {
    cursor: pointer;
  }

  @media ${breakpoints.md} {
    width: 157px;
  }
`;

const Container = styled.div`
  text-align: center;
  margin: 30px 0;
`;

export const ActiveDoneController = ({ active, setActive, setItemsToRender, category }) => {
  const activeProp = active === 1;
  const { t } = useTranslation('common');

  const activeLabel = t('activeCalls');
  const doneLabel = t('doneCalls');

  const handleStatusChange = React.useCallback(
    async (status) => {
      const categoryUrl = `https://ams.altius.digital/wp-json/v1/public-calls?status=${status}&limit=20&page=1&category=${category}`;
      const allCallsUrls = `https://ams.altius.digital/wp-json/v1/public-calls?status=${status}&limit=20&page=1`;
      const url = category ? categoryUrl : allCallsUrls;
      const res = await fetch(url);
      const publicCalls = await res.json();
      setItemsToRender(publicCalls);
      console.log(setActive);
      setActive(status);
    },
    [active, setActive, setItemsToRender]
  );

  return (
    <Container>
      <ButtonGroup>
        <StyledButtons active={activeProp} onClick={() => handleStatusChange(1)}>
          <Text>{activeLabel}</Text>
        </StyledButtons>
        <StyledButtons active={!activeProp} onClick={() => handleStatusChange(0)}>
          <Text>{doneLabel}</Text>
        </StyledButtons>
      </ButtonGroup>
    </Container>
  );
};
