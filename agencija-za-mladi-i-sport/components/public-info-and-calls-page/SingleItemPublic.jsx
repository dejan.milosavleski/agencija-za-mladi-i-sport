import React from 'react';
import Container from 'react-bootstrap/Container';
import styled from '@emotion/styled';
import { TitleArea } from '../common/TitleArea';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { breakpoints, theme } from '../../styles/theme';
import _map from 'lodash/map';
import _isEmpty from 'lodash/isEmpty';
import _get from 'lodash/get';
import { StyledHtml } from '../common/StyledHtml';
import { ResourceItem } from './ResourceItem';
import { useTranslation } from 'next-i18next';

const CategoryLabel = styled.div`
  font-size: 32px;
  font-family: StobiSans-Medium, sans-serif;
  text-align: center;
  margin-bottom: 20px;
`;

const Title = styled.div`
  color: ${theme.palette.navGray};
  font-size: 20px;
  font-family: StobiSans-Medium, sans-serif;
`;

const Divider = styled.hr`
  border: 2px solid ${theme.palette.primaryRed};
  width: 60px;
  margin-left: 10px;
  opacity: 1;
`;

const Status = styled.div`
  font-family: StobiSans-Bold, sans-serif;
  font-size: 20px;
`;

const ResourceContent = styled.div`
  margin: 20px 0;
`;

const Space = styled(Row)`
  height: 40px;
`;

const Label = styled.div`
  font-size: 18px;
`;

const Content = styled.div`
  font-size: 18px;
`;

const StickyContainer = styled.div`
  @media ${breakpoints.md} {
    margin: 30px 0;
  }
  padding-top: 10px;
  position: sticky;
  position: -webkit-sticky;
  top: 0px;
`;

const StyledCol = styled(Col)`
  @media ${breakpoints.md} {
    margin: 30px 0;
  }
`;

export const SingleItemPublic = ({ item, title, isPublicCall }) => {
  const { status, resources, valid_until, published_on } = item;
  const { t } = useTranslation('common');

  const statusLabel = t('statusLabel');
  const informationLabel = t('informationLabel');

  const activeLabel = status ? t('activeLabel') : t('doneLabel');
  const publishedLabel = t('publishedLabel');
  const untilLabel = t('untilLabel');

  const htmlContent = _get(item, 'content', '');

  return (
    <div>
      <Container>
        <TitleArea title={title} />
        <Row>
          <CategoryLabel>{item.title}</CategoryLabel>
        </Row>
        <Row>
          <StyledCol xs={12} md={3}>
            <StickyContainer>
              {isPublicCall && (
                <Row>
                  <Title>{statusLabel}</Title>
                  <Divider />
                  <Status>{activeLabel}</Status>
                </Row>
              )}
              <Space />
              <Row>
                <Title>{informationLabel}</Title>
                <Divider />
                {published_on && <Label>{`${publishedLabel}${published_on}`}</Label>}
                {valid_until && <Label>{`${untilLabel}${valid_until}`}</Label>}
              </Row>
            </StickyContainer>
          </StyledCol>
          <Col xs={12} sm={9} md={8}>
            <Content>
              <StyledHtml dangerouslySetInnerHTML={{ __html: htmlContent }} width="100%" />
            </Content>
            {!_isEmpty(resources) && (
              <ResourceContent>
                {_map(resources, (resource) => (
                  <ResourceItem resource={resource} />
                ))}
              </ResourceContent>
            )}
          </Col>
          <Col sm={0} md={1} />
        </Row>
      </Container>
    </div>
  );
};
