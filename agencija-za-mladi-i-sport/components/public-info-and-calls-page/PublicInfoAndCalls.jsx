import React from 'react';
import { TitleArea } from '../common/TitleArea';
import { Categories } from '../common/categories/Categories';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import styled from '@emotion/styled';
import _find from 'lodash/find';
import { ActiveDoneController } from './ActiveDoneController';
import { InfinityItems } from './InfinityItems';
import { LocaleContext } from '../../contexts/LocaleContext';
import { getCategoryTitle } from '../../utils';

const CategoryLabel = styled.div`
  font-size: 32px;
  font-family: StobiSans-Medium, sans-serif;
  text-align: center;
`;

export const PublicInfoAndCalls = ({
  title,
  items,
  firstPageName,
  categories,
  currentCategory,
  categoriesPageName,
  isCalls,
  fetchMoreItemsUrl,
  fetchMoreItemsFunctionUrl,
  subcategory
}) => {
  const currentCategoryObject = _find(categories, (cat) => cat.slug === currentCategory);
  const currentSubcategoryObject =
    subcategory && _find(currentCategoryObject.children[0], (sub) => sub.slug === subcategory);

  const categoryTitle = getCategoryTitle(currentCategoryObject, currentSubcategoryObject);

  const [active, setActive] = React.useState(1);
  const [itemsToRender, setItemsToRender] = React.useState(items);
  const { locale } = React.useContext(LocaleContext);

  return (
    <Container>
      <TitleArea title={title} />
      {(currentCategory || subcategory) && (
        <Row>
          <CategoryLabel>{categoryTitle}</CategoryLabel>
        </Row>
      )}
      {isCalls && (
        <Row>
          <ActiveDoneController
            active={active}
            setActive={setActive}
            setItemsToRender={setItemsToRender}
            category={currentSubcategoryObject ? currentSubcategoryObject.slug : currentCategory}
          />
        </Row>
      )}
      <Row>
        <Col xs={12} md={3}>
          <Categories
            firstPageName={categoriesPageName}
            currentCategory={currentCategory}
            currentSubcategory={subcategory}
            categories={categories}
            locale={locale}
          />
        </Col>
        <Col xs={12} md={9}>
          <InfinityItems
            fetchMoreItemsUrl={fetchMoreItemsUrl}
            fetchMoreItemsFunctionUrl={fetchMoreItemsFunctionUrl}
            initialItems={itemsToRender}
            firstPageName={firstPageName}
            status={active}
          />
        </Col>
      </Row>
    </Container>
  );
};
