import React from 'react';
import { StyledButton } from '../common/StyledButton';
import Image from 'next/image';
import look from '../../assets/icons/look.svg';
import download from '../../assets/icons/download.svg';
import styled from '@emotion/styled';
import { navigateTo } from '../../utils';
import { breakpoints, theme } from '../../styles/theme';
import { useTranslation } from 'next-i18next';

const StyledImage = styled.div`
  margin-right: 10px;
  padding-top: 3px;
  @media ${breakpoints.lg} {
    padding-top: 5px;
  }
`;

const ButtonContent = styled.div`
  display: inline-flex;
`;

const ButtonLabel = styled.div`
  @media ${breakpoints.lg} {
    padding-top: 5px;
  }
`;

const Label = styled.div`
  font-size: 18px;
  padding-top: 15px;
  flex: 3;
`;

const Buttons = styled.div`
  flex: 6;
  display: inline-flex;
  width: 100%;
  justify-content: end;
`;

const Content = styled.div`
  margin: 20px 0;
  display: inline-flex;
  border-bottom: 1px solid ${theme.palette.mediumGray};
  padding-bottom: 10px;
  width: 100%;
`;

export const ResourceItem = ({ resource }) => {
  const { download_link, resource_link, resource_title } = resource;

  const { t } = useTranslation('common');

  const lookLabel = t('lookLabel');
  const downloadLabel = t('downloadLabel');

  return (
    <Content>
      <Label>{resource_title}</Label>
      <Buttons>
        <StyledButton minwidth={245}>
          <ButtonContent>
            <StyledImage>
              <Image src={look} alt="arrow" width={15} height={15} onClick={() => navigateTo(resource_link)} />
            </StyledImage>
            <ButtonLabel>{lookLabel}</ButtonLabel>
          </ButtonContent>
        </StyledButton>
        <StyledButton minwidth={245} marginleft={10}>
          <ButtonContent>
            <StyledImage>
              <Image src={download} alt="arrow" width={15} height={15} onClick={() => navigateTo(download_link)} />
            </StyledImage>
            <ButtonLabel>{downloadLabel}</ButtonLabel>
          </ButtonContent>
        </StyledButton>
      </Buttons>
    </Content>
  );
};
