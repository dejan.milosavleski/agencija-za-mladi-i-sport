import React from 'react';
import styled from '@emotion/styled';

// Lodash
import _get from 'lodash/get';
import _map from 'lodash/map';
import _isEmpty from 'lodash/isEmpty';

// Components
import Link from 'next/link';
import Image from 'next/image';
import InfiniteScroll from 'react-infinite-scroll-component';
import { ListGroup } from 'react-bootstrap';
import { StyledIconImage } from '../common/StyledButton';
import { ScrollEndMessage } from '../common/ScrollEndMessage';

// Icons
import arrow from '../../assets/icons/arrow-red.svg';

// Context
import { LocaleContext } from '../../contexts/LocaleContext';
import { LoaderMessage } from '../common/LoaderMessage';
import { StyledInfiniteScroll } from '../common/StyledElements';

const StyledItem = styled(ListGroup.Item)`
  min-height: 60px;
  display: ${(props) => (props.first ? '' : 'inline-flex')};
  justify-content: ${(props) => (props.first ? 'none' : 'space-between')};
`;

const StyledList = styled(ListGroup)`
  padding: 10px;
`;

const StyledText = styled.div`
  font-size: 20px;
  font-family: StobiSans-Medium, sans-serif;
  padding-top: 8px;
  flex: 1;
`;

export const InfinityItems = ({
  fetchMoreItemsUrl,
  initialItems,
  firstPageName,
  fetchMoreItemsFunctionUrl,
  status
}) => {
  const [itemsToRender, setItemsToRender] = React.useState(initialItems);
  const [hasMoreItems, setHasMoreItems] = React.useState(initialItems.length >= 10);
  const [page, setPage] = React.useState(2);
  const { locale } = React.useContext(LocaleContext);

  React.useEffect(() => {
    setItemsToRender(initialItems);
  }, [initialItems]);

  const fetchMoreItems = async () => {
    let res;
    if (fetchMoreItemsUrl) {
      res = await fetch(`${fetchMoreItemsUrl}${page}`);
    } else {
      const url = fetchMoreItemsFunctionUrl(page, status);
      res = await fetch(url);
    }

    const items = await res.json();

    const updatedNews = itemsToRender.concat(items);
    setItemsToRender(updatedNews);
    setPage(page + 1);
    setHasMoreItems(!_isEmpty(items));
  };

  return (
    <StyledInfiniteScroll
      dataLength={_get(itemsToRender, 'length', 0)}
      next={fetchMoreItems}
      hasMore={hasMoreItems}
      loader={<LoaderMessage />}
      endMessage={<ScrollEndMessage />}
    >
      <StyledList variant="flush">
        {_map(itemsToRender, (item) => (
          <StyledItem key={item.ID}>
            <StyledText>
              <Link href={`/${firstPageName}/${item.slug}`} locale={locale}>
                {item.title}
              </Link>
            </StyledText>
            <Link href={`/${firstPageName}/${item.slug}`} locale={locale}>
              <StyledIconImage>
                <Image src={arrow} alt="arrow" width={15} height={15} />
              </StyledIconImage>
            </Link>
          </StyledItem>
        ))}
      </StyledList>
    </StyledInfiniteScroll>
  );
};
